# API Detail

## Table of Content

- [Metadata](#Metadata)
- [ข้อมูลดิบ](#NC-RAW-API)
- [ข้อมูลดัชนีสุดขั้ว](#NC-INDEX-API)

## Metadata

ข้อมูลรายละเอียดของทุก index และข้อมูล index ที่สนใจ

```
/api/indicesDetail
/api/indicesDetail/<index>
```

ข้อมูลของ dataset โดยไม่มีข้อมูลเกี่ยวกับพิกัด (เพื่อให้ frontend นำไปสร้าง select form) และ ข้อมูล dataset ที่สนใจ

```
/api/datasetsDetail/
/api/datasetsDetail/<dataset>
```

ข้อมูลพิกัดของ dataset ที่สนใจ

```
/api/datasetsDetail/<dataset>/grid
```

## NC RAW API

route:

```
/api/nc/datasets/<dataset>/types/<dataset_type>/indices/<index>/<start_date>/<end_date>
```

จะเป็นการ request ข้อมูลเฉลี่ยในช่วงเวลาที่เลือก(แผนที่) และข้อมูลกราฟ annual average, seasonal average

- `dataset` = โฟลเดอร์ dataset ที่เลือก
- `dataset_type` = โฟลเดอร์ย่อย (pathway - hist, rcp) ในโฟลเดอร์ย่อยที่เลือก
- `index` = index หรือ variable ที่สนใจ
- `start_date` และ `end_date` = วันที่ในรูปแบบ `yyyy-mm-dd`

การ request ข้อมูลแบบความละเอียดต่ำ

```
.../lowres
```

การ request ข้อมูล trend analysis

method คือ วิธีหาความชันต่างๆ

- `theilsen` = theil-sen estimator: ใช้เวลานาน (450s at 50year, 1.875x1.875)
- `linearreg` = linear regression: เร็วกว่า theil-sen (5s at 50year, 1.875x1.875)
- `leastsquare` = leastsquare: เร็วที่สุด (2s at 50year, 1.875x1.875)

```
.../mannkendall
.../mannkendall/lowres
.../slope/<method>
.../slope/<method>/lowres
```

การ request ข้อมูลกราฟเฉพาะพื้นที่ ทำได้โดยการส่ง header `country` และ `custom_polygon`

```
.../selectcountry
.../selectcustom
```

การ export ข้อมูลสามารถทำได้โดยการนำ `/exportcsv` ต่อท้าย

```
.../exportcsv
.../selectcountry/exportcsv
.../selectcustom/exportcsv
```

## NC INDEX API

route

```
/api/ncindices/datasets/<dataset>/indices/<index>/<start_year>/<end_year>
```

จะเป็นการ request ข้อมูลเฉลี่ยในช่วงเวลาที่เลือก(แผนที่) และข้อมูลกราฟ annual average (ไม่มี seasonal average เนื่องจาก index ถูกคำนวณแบบ annual)

การ request ข้อมูลแบบความละเอียดต่ำ

```
.../lowres
```

การ request ข้อมูล trend analysis

```
.../mannkendall
.../mannkendall/lowres
.../slope/<method>
.../slope/<method>/lowres
```

การ request ข้อมูลกราฟเฉพาะพื้นที่ ทำได้โดยการส่ง header `country` และ `custom_polygon`

```
.../selectcountry
.../selectcustom
```

การ export ข้อมูลสามารถทำได้โดยการนำ `/exportcsv` ต่อท้าย

```
.../exportcsv
.../selectcountry/exportcsv
.../selectcustom/exportcsv
```
