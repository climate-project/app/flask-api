# Climate Visualization web-app backend

- Back End ของโปรเจค **Bias Correction of Multi-Dimensional Climate Data and Visualization** (ปีการศึกษา 2562, ผู้จัดทำรหัส 59)
- พัฒนาโดย flask ซึ่งเป็นเฟรมเวิร์คภาษา python ใช้ database MongoDB ในการเก็บ metadata ของ Dataset และ อ่านข้อมูลจาก Dataset ที่เป็นไฟล์ nc เพื่อให้ frontend นำไปแสดงผล

### feature

- ค่าเฉลี่ยของข้อมูลที่ถูกเลือก (เชิงพื้นที่)
- trend analysis โดย `Mann-Kendall test (significance)`, `Theil-Sen Estimator (slope)` (เชิงพื้นที่)
- ข้อมูลพื้นที่ทั้งหมดมาเฉลี่ยเป็นรายปี `annual average` (time series)
- ข้อมูลพื้นที่ทั้งหมดมาเฉลี่ยเป็นรายเดือน `seasonal average`
- เลือกข้อมูลเฉพาะบางประเทศ หรือ ในพื้นที่ที่เลือก
- export ข้อมูลเป็น file csv

## Table of content

- [OS and Programming Language](#OS-and-Programming-Language)
- [Library](#Library)
- [Dataset](#Dataset)
- [Project Structure](#Project-Structure)
- [API](#API)
- [How to run server](#How-to-run-server)
- [Repository อื่นๆ ที่เกี่ยวข้องกับโปรเจค](#Another-Repository-by-Chuan)
- [แหล่งอ้างอิง](https://github.com/chuan-khuna/climate-simple-plot)

## OS and Programming Language

- เขียนโค้ดและพัฒนาใน windows 10
- python 3.6.7
- mongodb version 4.0.9

## Library

- ตัวอย่าง library สำคัญๆ ที่ใช้งานในโปรเจค

```
basemap==1.2.0
Flask==1.0.2
Flask-Compress==1.4.0
Flask-Cors==3.0.7
matplotlib==3.1.1
netCDF4==1.5.1.2
numpy==1.17.1
pandas==0.25.1
pymannkendall==1.2
Shapely==1.6.4.post2
seaborn==0.9.0
```

- library ทั้งหมดแสดงในไฟล์ requirements.txt

```
pip3 install -r requirements.txt
```

## Dataset

Dataset ส่วนมากถูกเก็บในรูปแบบไฟล์ `*.nc` (netcdf)

- แบ่งตาม RCM(Regional) และ GCM(Global)

```
Regional Climate Model: จะมีข้อมูลเฉพาะบางพื้นที่ และ มีความละเอียดของกริดมากกว่า (โดยประมาณ 0.25x0.25 degree)
```

```
Global Climate Model: จะมีข้อมูลครอบคลุมทั้งโลก และ มีความละเอียดของกริดที่น้อย (1.875x1.875 degree, 2.5x2.5 degree)
```

- แบ่งตาม Extreme Index และ Raw Data

```
Extreme Index (ดัชนีสุดขั้ว) : เป็นข้อมูลที่นำข้อมูลดิบ (Raw Data) ไปคำนวณแบบรายเดือน(monthly - 1 ปีมี 12 ค่า) หรือรายปี(annual - 1 ปีมี 1 ค่า) เช่น

TNn: อุณหภูมิต่ำสุดรายวันที่น้อยสุดในเดือนนั้นๆ
CDD: จำนวนวันต่อเนื่องที่ฝนไม่ตก
```

```
Raw Data: ข้อมูลดิบจากแบบจำลอง ข้อมูลที่สำคัญๆ ได้แก่ ข้อมูลอุณหภูมิและข้อมูลน้ำฝน
```

### Dataset ที่ใช้ (อ. เป็นคนดาวน์โหลดให้)

- Raw Data

```
MPI-ESM-MR (GCM)
EC-EARTH (RCM)
HadGEM2 (RCM)
MPI (RCM)
```

- Extreme Index จะถูกให้มาเป็นไฟล์ txt ต้องนำไปแปลงเป็น nc เอง

- Dataset ที่สามารถดาวน์โหลดมาเพื่อฝึกให้คุ้นเคย [Climate Index by Climdex](https://www.climdex.org/learn/datasets/)

## Project Structure

- ตำแหน่งการจัดเก็บไฟล์
- การทำงานคร่าวๆ ของโค้ด
- [more details](./Project-Structure.md)

## API

- รูปแบบ API
- [more details](./API-Detail.md)

## How to run server

- นำไฟล์ข้อมูลมาไว้วางตาม format ที่ถูกต้อง, หรือเพิ่มข้อมูลเกี่ยวกับ index ใน /app/ncfiles/\_data
- ทำการ import meatadata เข้า mongo db โดยรันไฟล์ python ใน `/app/ncfiles/` **ข้อควรระวัง**ต้องดูรายละเอียดของแต่ละ dataset
- เปลี่ยน port ได้ที่ไฟล์ `runserver.py` **(`threaded=True` ใช้แก้ปัญหาเมื่อเรียก API request ในเวลาใกล้เคียงกันมากๆ(พร้อมกัน) เหมือน flask จะรับได้ทีละ request)**

```python
python3 runserver.py
```

เซิฟเวอร์จะอยู่ที่พอร์ต

```
localhost:3000/
```

## Another Repository by Chuan

### [Bias Correction v2](https://gitlab.com/climate-project/bias-correction/bias_correction2)

- การทำ Bias Correction โดยอ่านจาก nc file โดยตรง (v1 เป็นการ export จาก nc มาที่ csv ก่อน)
- ข้อมูลวัดจริงอ่านจาก CSV

### [BiasCorrectionLib](https://gitlab.com/climate-project/bias-correction/BiasCorrectionLib)

- ไลบรารี่ Bias correction อย่างง่ายที่เขียนเอง สำหรับใช้ใน Bias correction v2

### [จัดข้อมูลให้อยู่ใน format ที่ต้องการ](https://gitlab.com/climate-project/curation_csv)

- จัดข้อมูลให้อยู่ใน format ตามความต้องการของ อ. แล้วก็ได้นำไปใช้ใน Bias Correction v2

### [Qmap Bias correction](https://gitlab.com/climate-project/bias-correction/bias_correction_qmap)

- ใช้ Qmap (R Library) ในการทำ Bias Correction

### [Bias Correction Climdex](https://gitlab.com/climate-project/bias-correction/bias_correction_and_climdex)

- ดูผลลัพธ์ของการทำ Bias correction ในด้านของการนำไปคำนวณ climdex ด้วย
- จัด format ข้อมูลเพื่อให้โปรแกรม Climpact ประมวลผลเป็น index ได้

#### [txt_indices_to_nc](https://gitlab.com/climate-project/txt_indices_to_nc)

- แปลงข้อมูล index จาก text file ให้เป็น nc file

#### [repository ตอนเริ่มต้นทำความเข้าใจข้อมูล climate](https://gitlab.com/climate-project/climate-simple-plot)

- repository ตอนเริ่มทำโปรเจคใหม่ๆ ทำความเข้าใจมุมมองต่างๆ ของ climate
- แหล่งอ้างอิงที่ใช้ในการทำโปรเจค

#### [my lib](https://gitlab.com/climate-project/bias-correction/myproject_lib)
- โค้ดที่ใช้ซ้ำๆ 
- พล็อตแมพด้วย matplotlib
- การ shift ข้อมูล
- การทำให้กริดหยาบมากขึ้น

#### [สรุปการ deploy](./deploy_guide.md)
