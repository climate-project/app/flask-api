from flask_cors import CORS
from flask import jsonify, request, Blueprint
from flask import send_file
from app import app
from app.nc_routes import nc_routes
from app.index_routes import index_routes
from app.ncindices_routes import ncindices_routes
import json

import numpy as np
from copy import deepcopy
import datetime
import ast
import os

# ------------------------------
# import my lib
# ------------------------------
from .lib.NCFile import NCFile
from .lib.mymongo import Mongo

# for mongo trend analysis
from .lib import spatial
from .lib import spatial_trend_analysis

# for nc trend analysis
from .lib import my_ncquery
from .lib import spatial_nc

app.config["CORS_HEADERS"] = "Content-Type"

database = "climatedb_2019"


app.register_blueprint(nc_routes)
app.register_blueprint(index_routes)
app.register_blueprint(ncindices_routes)

cors = CORS(app, resources={"/*": {"origins": ["*"]}})

@app.route("/api/indicesDetail")
def indices_detail():
    db = Mongo(database=database, collection="index_detail")
    result = db.find({})
    return jsonify(result)


# get details of index for show in detail section
@app.route("/api/indicesDetail/<index>")
def index_detail(index):
    db = Mongo(database=database, collection="index_detail")
    result = db.find({"index": index})
    return jsonify(result)


# get brief details of datasets (with out coordinate)
# for select option
@app.route("/api/datasetsDetail/")
def datasets_detail():
    db = Mongo(database=database, collection="dataset")
    result = db.find({}, {"_id": 0, "lat": 0, "lon": 0, "gridcenter": 0, "geojson_gridcenter": 0, "gridsize": 0, "lowres": 0})
    return jsonify(result)


# get brief details of one dataset
# for show in detail section
@app.route("/api/datasetsDetail/<dataset>")
def dataset_detail(dataset):
    db = Mongo(database=database, collection="dataset")
    result = db.find({"dataset": dataset}, {"_id": 0, "lat": 0, "lon": 0, "gridcenter": 0, "geojson_gridcenter": 0, "gridsize": 0, "lowres": 0})
    return jsonify(result)


# api for get dataset grid polygon coordinate
@app.route("/api/datasetsDetail/<dataset>/grid")
def dataset_coordinate(dataset):
    db = Mongo(database=database, collection="dataset")
    result = db.find({"dataset": dataset}, {"_id": 0, "source": 0, "dataset_type": 0, "dataset": 0, "types": 0, "year": 0, "gridcenter": 0})
    return jsonify(result)


# ------ API for nc file ------
@app.route("/ncfiles")
def ncfiles():
    folders = ["ghcndex_test", "hadex2_test"]
    result = []
    for folder in folders:
        path = f"./app/ncfiles/{folder}"
        files = os.listdir(path)
        result.append({"folder": folder, "files": files})
    return jsonify(result)


@app.route("/ncfiles/<folder>/<file_name>", methods=["PUT"])
def update_ncfile(folder, file_name):
    path = f"./app/ncfiles/{folder}/{file_name}"
    content = request.json
    ncfile = NCFile(path)
    r = ncfile.edit_grid(
        time_var=content["month"],
        year_ind=content["year_ind"],
        row_ind=content["row_ind"],
        col_ind=content["col_ind"],
        val=content["val"],
    )
    return jsonify(r)


# test return image file
@app.route("/getimage")
def send_image():
    return send_file("./lib/data/result_1577257473.png", mimetype="image/png")
