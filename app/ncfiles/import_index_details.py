import pymongo
from netCDF4 import Dataset
import numpy as np
import pandas as pd


def import_index_details(collection, detail_file):
    """[import index details from detail_file.csv to mongodb collection]
    
    Arguments:
        collection {[mongo collection]} -- [mongo collection obj]
        detail_file {[string]} -- [path/to/file]
    """

    df = pd.read_csv(detail_file)

    for row in df.iterrows():
        index_meaning = {
            "index": row[1]['index'],
            "short_definition": row[1]['short'],
            "definition": row[1]['definition'],
            "unit": row[1]['unit'],
            "type": row[1]['type'],
            "measurement": row[1]['measurement'],
            "color_map": row[1]['color_map'],
        }

        q = list(
            collection.find({
                'index': index_meaning['index'],
                'short_definition': index_meaning['short_definition']
            }))
        if len(q) > 0:
            collection.update(q[0], index_meaning, upsert=True)
        else:
            collection.insert_one(index_meaning)
        print(f"create: {index_meaning['index']} ({index_meaning['unit']}) \t {index_meaning['short_definition']}")


if __name__ == '__main__':
    client = pymongo.MongoClient("mongodb://localhost:27017/")
    dbname = "climatedb_2019"
    db = client[dbname]
    collection = db['index_detail']

    import_index_details(collection, "_data/index_definitions.csv")