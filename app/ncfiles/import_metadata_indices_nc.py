import pymongo
import os
import datetime
from netCDF4 import Dataset
import numpy as np
import sys
sys.path.append("..") # Adds higher directory to python modules path.
from _metadata_lib.shift import shift_lon
from _metadata_lib.cartesian_product import orderpair
from _metadata_lib.geojson_gen import gen_geojson_polygons, gen_geojson_points
from lib.upscaling import *


def import_metadata(collection, dataset_name):
    source_path = f"./nc_indices/{dataset_name}"

    indices_dict = []
    for filename in os.listdir(source_path):
        ds = Dataset(f"{source_path}/{filename}")
        index = filename.split('_')[2][:-3]
        months = list(ds.variables.keys())[3:]
        indices_dict.append({'index': index, 'month': months})

    lons = np.array(ds['lon'][:])
    lats = np.array(ds['lat'][:])
    lons = shift_lon(np.round(lons, 5))[0]
    lats = np.round(lats, 5)

    grid_center = orderpair(lons, lats)
    geojson_gridcenter = gen_geojson_points(grid_center)

    lon_step = np.nanmean((lons - np.roll(lons, 1))[1:])
    lat_step = np.nanmean((lats - np.roll(lats, 1))[1:])

    lowres_lons, lowres_lats = upscaling_lonlat(lons, lats)
    lowres_gridcenter = orderpair(lowres_lons, lowres_lats)
    lowres_geojson_gridcenter = gen_geojson_points(lowres_gridcenter)
    lowres_lon_step = np.nanmean((lowres_lons - np.roll(lowres_lons, 1))[1:])
    lowres_lat_step = np.nanmean((lowres_lats - np.roll(lowres_lats, 1))[1:])

    year_list = np.array(ds['time'][:])

    dataset = {
        "source": "nc",
        "dataset_type": "index",
        "dataset": dataset_name,
        "lon": lons.tolist(),
        "lat": lats.tolist(),
        "year": year_list.tolist(),
        "gridcenter": grid_center.tolist(),
        "gridsize": {
            "lon_step": np.round(lon_step, 2),
            "lat_step": np.round(lat_step, 2)
        },
        "indices": indices_dict,
        "geojson_gridcenter": geojson_gridcenter,
        "lowres": {
            "lon": lowres_lons.tolist(),
            "lat": lowres_lats.tolist(),
            "gridsize": {
                "lon_step": np.round(lowres_lon_step, 2),
                "lat_step": np.round(lowres_lat_step, 2),
            },
            "geojson_gridcenter": lowres_geojson_gridcenter,
            "gridcenter": lowres_gridcenter.tolist(),
        }
    }

    q = list(
        collection.find({
            'dataset': dataset['dataset'],
            'source': dataset['source'],
            "dataset_type": dataset['dataset_type']
        }))
    if len(q) > 0:
        collection.update(q[0], dataset, upsert=True)
    else:
        collection.insert_one(dataset)
    print(f"created: {dataset['dataset']}")


if __name__ == "__main__":
    client = pymongo.MongoClient("mongodb://localhost:27017/")
    # create DB if not exist and connect to it
    dbname = "climatedb_2019"
    db = client[dbname]
    # create collection
    collection = db["dataset"]
    DATASET_NAME = "mpi_rcp85"

    import_metadata(collection, DATASET_NAME)