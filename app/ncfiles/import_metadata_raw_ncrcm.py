import pymongo
import os
import datetime
from netCDF4 import Dataset
import numpy as np
import sys
sys.path.append("..") # Adds higher directory to python modules path.
from _metadata_lib.shift import shift_lon
from _metadata_lib.cartesian_product import orderpair
from _metadata_lib.geojson_gen import gen_geojson_polygons, gen_geojson_points
from lib.upscaling import *

def import_metadata(collection, dataset_name, indices):
    """
        import metadata from folder dataset_name to database collection
        
        example: dataset_name = MPI (RCM model)
            MPI
                /hist
                  ... nc here ...
                /rcp45
                /rcp85
        
        indices: index list that you want to import to metadata 
        (should call variable in raw data), such as tas, tasmax, pr
    """

    # path to dataset folder
    source_path = f"./nc_raw/{dataset_name}/"

    # subfolder in dataset folder
    dataset_types = os.listdir(source_path)

    # ----------------------------------------------------------------
    # import subfolder details
    # start and end date
    # ----------------------------------------------------------------
    data_type_dict = []
    for dataset_type in dataset_types:
        path = f"{source_path}/{dataset_type}/"

        files = os.listdir(path)

        # first and last file in type for calculate date range
        fist_ds = Dataset(f"{path}/{files[0]}")
        last_ds = Dataset(f"{path}/{files[-1]}")

        # calculate date range
        first_date_offset = datetime.datetime.strptime(fist_ds["time"].units.split(" ")[2], "%Y-%m-%d")
        last_date_offset = datetime.datetime.strptime(last_ds["time"].units.split(" ")[2], "%Y-%m-%d")

        start_date = first_date_offset + datetime.timedelta(np.asscalar(fist_ds["time"][0].data/24 - 1))
        end_date = last_date_offset + datetime.timedelta(np.asscalar(last_ds["time"][-1].data/24 - 1))

        # type details
        type_dict = {
            "type": dataset_type,
            "start_date": start_date.strftime("%Y-%m-%d"),
            "end_date": end_date.strftime("%Y-%m-%d"),
            "indices": indices,
        }

        data_type_dict.append(type_dict)

    # ----------------------------------------------------------------
    # import coordinates details
    # ----------------------------------------------------------------

    path = f"{source_path}/{dataset_types[0]}/"
    files = os.listdir(path)

    # select one file from dataset
    ds = Dataset(f"{path}/{files[0]}")

    try:
        lats = np.round(np.array(ds["lat"][:]), 4)
        lons = np.round(np.array(ds["lon"][:]), 4)
    except:
        lats = np.round(np.array(ds["xlat"][:, 0]), 4)
        lons = np.round(np.array(ds["xlon"][0]), 4)
    
    lons, shift_index = shift_lon(lons)
    grid_center = orderpair(lons, lats)
    geojson_gridcenter = gen_geojson_points(grid_center)
    lon_step = np.nanmean((lons - np.roll(lons, 1))[1:])
    lat_step = np.nanmean((lats - np.roll(lats, 1))[1:])

    lowres_lons, lowres_lats = upscaling_lonlat(lons, lats)
    lowres_gridcenter = orderpair(lowres_lons, lowres_lats)
    lowres_geojson_gridcenter = gen_geojson_points(lowres_gridcenter)
    lowres_lon_step = np.nanmean((lowres_lons - np.roll(lowres_lons, 1))[1:])
    lowres_lat_step = np.nanmean((lowres_lats - np.roll(lowres_lats, 1))[1:])


    dataset = {
        "source": "nc",
        "dataset_type": "raw",
        "dataset": dataset_name,
        "types": data_type_dict,
        "lat": lats.tolist(),
        "lon": lons.tolist(),
        "gridcenter": grid_center.tolist(),
        "gridsize": {
            "lon_step": np.float64(lon_step),
            "lat_step": np.float64(lat_step)
        },
        "geojson_gridcenter": geojson_gridcenter,
        "lowres": {
            "lon": lowres_lons.tolist(),
            "lat": lowres_lats.tolist(),
            "gridsize": {
                "lon_step": np.round(lowres_lon_step, 2),
                "lat_step": np.round(lowres_lat_step, 2),
            },
            "geojson_gridcenter": lowres_geojson_gridcenter,
            "gridcenter": lowres_gridcenter.tolist(),
        }
    }

    q = list(collection.find({'dataset': dataset['dataset'], 'source': dataset['source']}))
    if len(q) > 0:
        collection.update(q[0], dataset, upsert=True)
    else:
        collection.insert_one(dataset)
    print(f"created: {dataset['dataset']}")

if __name__ == "__main__":

    client = pymongo.MongoClient("mongodb://localhost:27017/")
    # create DB if not exist and connect to it
    dbname = "climatedb_2019"
    db = client[dbname]
    # create collection
    collection = db["dataset"]

    DATASET_NAME = "HadGEM2"

    dataset_type = "rcp45"
    files = os.listdir(f"./nc_raw/{DATASET_NAME}/{dataset_type}/")
    ds = Dataset(f"./nc_raw/{DATASET_NAME}/{dataset_type}/{files[0]}")

    INDICES = list(ds.variables.keys())[-4:]

    import_metadata(collection, DATASET_NAME, INDICES)