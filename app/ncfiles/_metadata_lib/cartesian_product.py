import numpy as np


def orderpair(x, y):
    return np.array(np.transpose([np.tile(x, len(y)), np.repeat(y, len(x))]))