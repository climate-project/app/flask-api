import numpy as np


def gen_geojson_polygons(polygon_coordinates_arr):
    geojson_polygons = []
    for i, poly in enumerate(polygon_coordinates_arr):
        geojson_polygons.append({
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": np.round(poly, 4).tolist()
            }
        })

    geojson_dict = {'type': 'FeatureCollection', 'features': geojson_polygons}

    return geojson_dict


def gen_geojson_points(points_coordinates_arr):
    geojson_points_features = []
    for i in points_coordinates_arr:
        geojson_points_features.append({"type": "Feature", "geometry": {"type": "Point", "coordinates": i.tolist()}})
    geojson_points_features = {"type": "FeatureCollection", "features": geojson_points_features}
    return geojson_points_features