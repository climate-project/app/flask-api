from netCDF4 import Dataset
import numpy as np

class NCFile:
    def __init__(self, path):
        self.dataset = Dataset(path, 'r+')
    
    def edit_grid(self, time_var, year_ind, row_ind, col_ind, val):
        try:
            old = self.dataset.variables[time_var][year_ind][row_ind][col_ind]
            # clone to data
            data = self.dataset.variables[time_var][year_ind]
            # edit in data
            data[row_ind][col_ind] = val
            # copy back to original
            self.dataset.variables[time_var][year_ind] = data[:]
            new = self.dataset.variables[time_var][year_ind][row_ind][col_ind]
            self.dataset.close()
            return {
                    "Status": "Success",
                    "old_val": np.array(old).tolist(),
                    "new_val": np.array(new).tolist()
                }
        except:
            return {"Status": "Fail"}