import numpy as np
import warnings
import json

from shapely.geometry import Point
from shapely.geometry.polygon import Polygon


def _get_country_mask_arr(country, lats, lons):
    with open('./app/lib/data/geo-medium.json') as json_file:
        jsondata = json.load(json_file)
        for c in jsondata['features']:
            if c['properties']['name'] == country:
                country_json = c
                break

    country_polygon = country_json['geometry']['coordinates']

    lats = np.array(lats)
    lons = np.array(lons)
    lats_step = np.abs(lats[0] - lats[1])
    lons_step = np.abs(lons[0] - lons[1])
    # create (lon, lat) order-pair
    grid_center_coor = np.transpose([np.tile(lons, len(lats)), np.repeat(lats, len(lons))])
    # create mask array
    mask_arr = np.zeros(len(lats) * len(lons))

    for i, coordinate in enumerate(grid_center_coor):
        # center
        point1 = Point(coordinate)
        # edge
        point2 = Point([coordinate[0] - lons_step / 2, coordinate[1] - lons_step / 2])
        point3 = Point([coordinate[0] - lons_step / 2, coordinate[1] + lons_step / 2])
        point4 = Point([coordinate[0] + lons_step / 2, coordinate[1] - lons_step / 2])
        point5 = Point([coordinate[0] + lons_step / 2, coordinate[1] + lons_step / 2])

        for p in country_polygon:
            try:
                p = np.squeeze(p, axis=0)
            except:
                p
            polygon = Polygon(p)
            if polygon.contains(point1) or polygon.contains(point2) or polygon.contains(point3) or polygon.contains(
                    point4) or polygon.contains(point5):
                mask_arr[i] = 1
                break

    mask_arr = mask_arr.reshape(len(lats), len(lons))

    return mask_arr


def _get_custom_mask_arr(polygon_coor, lats, lons):
    lats = np.array(lats)
    lons = np.array(lons)
    lats_step = np.abs(lats[0] - lats[1])
    lons_step = np.abs(lons[0] - lons[1])
    # create (lon, lat) order-pair
    grid_center_coor = np.transpose([np.tile(lons, len(lats)), np.repeat(lats, len(lons))])
    # create mask array
    mask_arr = np.zeros(len(lats) * len(lons))

    for i, coordinate in enumerate(grid_center_coor):
        # center
        point1 = Point(coordinate)
        # edge
        point2 = Point([coordinate[0] - lons_step / 2, coordinate[1] - lons_step / 2])
        point3 = Point([coordinate[0] - lons_step / 2, coordinate[1] + lons_step / 2])
        point4 = Point([coordinate[0] + lons_step / 2, coordinate[1] - lons_step / 2])
        point5 = Point([coordinate[0] + lons_step / 2, coordinate[1] + lons_step / 2])

        polygon = Polygon(polygon_coor)
        if polygon.contains(point1) or polygon.contains(point2) or polygon.contains(point3) or polygon.contains(
                point4) or polygon.contains(point5):
            mask_arr[i] = 1

    mask_arr = mask_arr.reshape(len(lats), len(lons))
    return mask_arr