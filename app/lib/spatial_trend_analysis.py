import numpy as np
import pymannkendall as mk
from sklearn.linear_model import TheilSenRegressor, LinearRegression


def mk_test(spatial_data):
    significance = []
    for row in range(spatial_data.shape[1]):
        row_data = []
        for col in range(spatial_data.shape[2]):
            data = spatial_data[:, row, col]
            # drop nan
            data = data[~np.isnan(data)]
            # calculate significance if data is available more than 70%, and more than 2 year
            if len(data) / len(spatial_data[:, row, col]) >= 0.7 and len(data) >= 2:
                mkt = mk.original_test(data, alpha=0.05)
                mkt_hypo_res = mkt.h
            else:
                mkt_hypo_res = False
            row_data.append(mkt_hypo_res)
        significance.append(row_data)
    return np.array(significance)


def mk_test2(spatial_data):
    significance = []
    try:
        for row in range(spatial_data.shape[1]):
            row_data = []
            for col in range(spatial_data.shape[2]):
                data = spatial_data[:, row, col]
                # drop nan
                data = data[~np.isnan(data)]
                # calculate significance if data is available more than 70%, and more than 2 year
                if len(data) / len(spatial_data[:, row, col]) >= 0.7 and len(data) >= 2:
                    mkt = mk.original_test(data, alpha=0.05)
                    if mkt.trend == "increasing":
                        trend_sign = 1
                    elif mkt.trend == "decreasing":
                        trend_sign = -1
                    else:
                        trend_sign = 0
                else:
                    trend_sign = 0
                row_data.append(trend_sign)
            significance.append(row_data)
    except:
        significance = np.zeros([spatial_data.shape[1], spatial_data.shape[2]])
    return np.array(significance)


def theil_sen(spatial_data):
    slopes = []
    for row in range(spatial_data.shape[1]):
        row_data = []
        for col in range(spatial_data.shape[2]):
            data = spatial_data[:, row, col]
            # drop nan data
            data = data[~np.isnan(data)]
            # calculate trend if data is available > 70%, and more than 2 year
            if len(data) / len(spatial_data[:, row, col]) >= 0.7 and len(data) >= 2:
                estimator = TheilSenRegressor(max_iter=100)
                x = np.arange(1, len(data) + 1)
                estimator.fit(x.reshape(-1, 1), data)
                slope = estimator.coef_[0]
            else:
                slope = None
            row_data.append(slope)
        slopes.append(row_data)
    return np.array(slopes, dtype=np.float)


def linear_reg(spatial_data):
    slopes = []
    for row in range(spatial_data.shape[1]):
        row_data = []
        for col in range(spatial_data.shape[2]):
            data = spatial_data[:, row, col]
            # drop nan data
            data = data[~np.isnan(data)]
            # calculate trend if data is available > 70%, and more than 2 year
            if len(data) / len(spatial_data[:, row, col]) >= 0.7 and len(data) >= 2:
                estimator = LinearRegression()
                x = np.arange(1, len(data) + 1)
                estimator.fit(x.reshape(-1, 1), data)
                slope = estimator.coef_[0]
            else:
                slope = None
            row_data.append(slope)
        slopes.append(row_data)
    return np.array(slopes, dtype=np.float)


class LeastSquare:
    """
        Least Square:
            h = w0x0 + w1x1 + ....
            parameters = inverse((X.T) * X) * (X.T * y)   ; x*y = x dot y
            parameters = [w0 w1 w2 ...]
        X = [
            [x0 x1 ...]
            [x0 x1 ...]
            ...
        ]
        y = [
            y0
            y1
            ...
        ]
    """

    def __init__(self, x, y):
        self.x = x
        self.expected_y = y
        self.parameters = []
        self.least_square()

    def least_square(self):
        x_t = self.x.transpose()
        self.parameters = np.linalg.inv(x_t.dot(self.x)).dot(x_t.dot(self.expected_y))

        self.parameters = np.around(self.parameters, 4)


def least_square(spatial_data):
    slopes = []
    for row in range(spatial_data.shape[1]):
        row_data = []
        for col in range(spatial_data.shape[2]):
            data = spatial_data[:, row, col]
            # drop nan data
            data = data[~np.isnan(data)]
            # calculate trend if data is available > 70%, and more than 2 year
            if len(data) / len(spatial_data[:, row, col]) >= 0.7 and len(data) >= 2:
                x = np.arange(1, len(data) + 1)
                x = np.vstack([np.ones(len(x)), x]).T
                estimator = LeastSquare(x, data)
                slope = estimator.parameters[1]
            else:
                slope = None
            row_data.append(slope)
        slopes.append(row_data)
    return np.array(slopes, dtype=np.float)
