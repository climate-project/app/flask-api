import numpy as np


def roundlist(arr, round_digit=2):
    arr = np.round(np.array(arr, dtype=np.float), round_digit)
    if round_digit == 0:
        arr = np.array(arr, dtype=np.int)
    return np.where(np.isnan(arr), None, arr).tolist()
