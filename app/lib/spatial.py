import numpy as np
import warnings
from .mask_selected import _get_country_mask_arr, _get_custom_mask_arr

def annual_avg(data, startmonth, stopmonth):
    """
        calculate annual avg of query_data(spatial)
        if will return np array of annual_avg-spatial_data
    """

    # check if query data is monthly-index(jan...dec + ann) or annual index (ann only)
    if np.array(data).shape[1] != 1:
        # data is monthly type (jan...dec + ann)

        # check if user select ann month or default month
        # if select ann month in start and stop => will calculate only ann month
        if startmonth != 12 and stopmonth != 12:
            data[0] = data[0][startmonth:]
            data[-1] = data[-1][:stopmonth + 1]
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning)
                spatial_ann_avg = [
                    np.nanmean(np.array(i, dtype=np.float), axis=0)
                    for i in data
                ]
        else:
            # select only ann month
            data = [i[-1] for i in data]
            spatial_ann_avg = data
    else:
        # data is annual type (ann only)
        data = [i[-1] for i in data]
        spatial_ann_avg = data

    spatial_ann_avg = np.array(spatial_ann_avg, dtype=np.float)

    return spatial_ann_avg

def spatial_period_avg(spatial_annual_avg):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        return np.nanmean(spatial_annual_avg, axis=0)

def monthly_avg(data, startmonth, stopmonth):
    monthly_avg_data = []
    # check if query data is monthly-index(jan...dec + ann) or annual index (ann only)
    if np.array(data).shape[1] != 1:
        # data is monthly type (jan...dec + ann)

        # check if user select ann month or default month
        # if select ann month in start and stop => will calculate only ann month
        if startmonth != 12 and stopmonth != 12:
            for y in data:
                dataYear = np.array(y, dtype=np.float)
                month = []
                for m in range(1,len(dataYear)):
                    temp = dataYear[m]
                    value = np.nanmean(temp)
                    month.append(value)
                monthly_avg_data.append(month)
           
        else:
            # select only ann month
            pass
    else:
        # data is annual type (ann only)
        pass
    
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        monthly_avg_data = np.nanmean(np.array(monthly_avg_data, dtype=np.float), axis=0)

    return np.round(monthly_avg_data, 2)

def global_avg(spatial_annual_avg):
    global_avg = [np.nanmean(i) for i in spatial_annual_avg]
    return np.round(global_avg, 2)

def mask_inside_country(country, lats, lons, data):

    lats = np.array(lats)
    lons = np.array(lons)
    mask_arr = _get_country_mask_arr(country, lats, lons)

    for y in range(len(data)):
        for m in range(len(data[y])):
            data[y][m] = np.where(mask_arr==1, np.array(data[y][m], dtype=np.float), np.nan)

    return data

def mask_inside_polygon(polygon_coor, lats, lons, data):

    lats = np.array(lats)
    lons = np.array(lons)
    mask_arr = _get_custom_mask_arr(polygon_coor, lats, lons)

    for y in range(len(data)):
        for m in range(len(data[y])):
            data[y][m] = np.where(mask_arr==1, np.array(data[y][m], dtype=np.float), np.nan)

    return data

def mask_inside_country_daily(country, lats, lons, data):

    lats = np.array(lats)
    lons = np.array(lons)
    mask_arr = _get_country_mask_arr(country, lats, lons)

    for y in range(len(data)):
        for m in range(len(data[y])):
            for d in range(len(data[y][m])):
                data[y][m][d] = np.where(mask_arr==1, np.array(data[y][m][d], dtype=np.float), np.nan)

    return data

def mask_inside_polygon_daily(polygon_coor, lats, lons, data):

    lats = np.array(lats)
    lons = np.array(lons)
    mask_arr = _get_custom_mask_arr(polygon_coor, lats, lons)

    for y in range(len(data)):
        for m in range(len(data[y])):
            for d in range(len(data[y][m])):
                data[y][m][d] = np.where(mask_arr==1, np.array(data[y][m][d], dtype=np.float), np.nan)

    return data