import pymongo
from bson.json_util import dumps


class Mongo():
    def __init__(self, database, collection, url="localhost", port=27017):
        full_url = ""
        self.client = pymongo.MongoClient(url, port)
        self.db = self.client[database]
        self.collection = self.db[collection]

    def find(self, condition, option={'_id': 0}):
        result = self.collection.find(condition, option)
        # collection.find ruturn Mongo 'Cursor' Type
        return list(result)


if __name__ == "__main__":
    db = Mongo(database="myclimatedb", collection="index_meaning")
    index = "TNx"
    res = db.find({"index": index})
    print(res)
    res = db.find({})
    print(res)

    db = Mongo(database="myclimatedb", collection="index")
    res = db.find({"index": "TNx"})
    print(res)
