import numpy as np
import pandas as pd
from copy import deepcopy

def monthly_avg(query_data):
    """
        avg value calculate from mean all days of month
        data_axis: y, m, [#month_avg_spatial]
    """

    year_data = []
    for year in range(query_data.shape[0]):
        month_data = []
        for month in range(query_data.shape[1]):
            month_avg = np.nanmean(query_data[year][month], axis=0)
            month_data.append(month_avg)
        year_data.append(month_data)

    return np.round(np.array(year_data, dtype=np.float), 4)

def annual_avg(monthly_avg_data):
    return np.round(np.nanmean(monthly_avg_data, axis=1), 4)

def maskgrid(monthly_avg_data, file_path):
    land_data = deepcopy(monthly_avg_data)
    mask_arr = np.array(pd.read_csv(file_path))
    for y in range(len(land_data)):
        for m in range(len(land_data[y])):
            land_data[y][m] = np.where(mask_arr==1, land_data[y][m], np.nan)

    sea_data = deepcopy(monthly_avg_data)
    for y in range(len(sea_data)):
        for m in range(len(sea_data[y])):
            sea_data[y][m] = np.where(mask_arr==0, sea_data[y][m], np.nan)
    return land_data, sea_data

def period_avg(monthly_avg_data):

    return np.round(np.nanmean(monthly_avg_data, axis=(0, 1)), 4)

def area_seasonal_avg(monthly_avg_data):

    return np.round(np.nanmean(monthly_avg_data, axis=(2, 3, 0)), 4)


def area_annual_avg(monthly_avg_data):

    return np.round(np.nanmean(monthly_avg_data, axis=(2, 3, 1)), 4)

def area_monthly_avg(monthly_avg_data):

    return np.round(np.nanmean(monthly_avg_data, axis=(2, 3)), 4)