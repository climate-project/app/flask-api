import numpy as np
from netCDF4 import Dataset
import os


def query_period_data(dataset_path, dataset_type, index, start_date, end_date, shift_index):
    selected_year = list(range(start_date.year, end_date.year + 1))
    selected_file = [
        file_name for file_name in os.listdir(f"{dataset_path}/{dataset_type}")
        if int(file_name[-9:-5]) in selected_year
    ]

    # ----------------------------
    # data_axis = y, m, d, [#spatial]
    # ----------------------------
    period_data = []
    for file_name in selected_file:
        ds = Dataset(f"{dataset_path}/{dataset_type}/{file_name}", mode='r')
        # from mask array to array
        # data is np array dtype = float
        data = ds[index][:].filled().squeeze()
        ds.close()
        try: 
            # get mask array filled value
            fill_val = ds[index]._FillValue
            # replace fill value with nan
            data = np.where(data == fill_val, np.nan, data)
        except:
            data = np.array(data, dtype=np.float)
        data = np.round(data, 4)

        # shift data from lat -90..90, lon 0..360
        # to lat -90..90, lon -180..180 (same as lat, lon coordinate in mongodb)
        for i in range(len(data)):
            data[i] = _shift_data(data[i], shift_index)


        period_data.append(data)
    period_data = np.array(period_data)
    try:
        period_data = period_data.reshape(period_data.shape[0] // 12, 12)
    except:
        # if 1 year = 360 day in HadGEM2
        period_data = period_data.reshape(period_data.shape[0]//12, 12, period_data.shape[1], period_data.shape[2], period_data.shape[3])
    # ----------------------------
    # data_axis = y, m, d, [#spatial]
    #
    # not seleted date will fill with nan
    # ----------------------------

    # fill nan before start month
    for i in range(start_date.month - 1):
        period_data[0][i].fill(np.nan)
    # fill nan after end month
    for i in range((end_date.month - 1) + 1, 12):
        period_data[-1][i].fill(np.nan)

    # fill nan before start date of start month
    for i in range(start_date.day - 1):
        period_data[0][start_date.month - 1][i].fill(np.nan)
    # fill nan after end date of end month
    for i in range((end_date.day - 1) + 1, period_data[-1][end_date.month - 1].shape[0]):
        period_data[-1][end_date.month - 1][i].fill(np.nan)

    # get unit
    ds = Dataset(f"{dataset_path}/{dataset_type}/{selected_file[0]}")
    unit = ds[index].units

    # Convert index unit from K to C and pr_unit to mm/day
    if unit == 'K':
        period_data = period_data - 273.15
    elif unit == 'kg m-2 s-1':
        period_data = period_data * 86400

    return period_data

def _shift_data(data, shift_index):
    if shift_index:
        data = np.roll(data, shift_index, axis=1)
        return data
    else:
        return data