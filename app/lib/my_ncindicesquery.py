from netCDF4 import Dataset
import numpy as np
import os

def query_index_period_data(dataset_path, index, start_year, end_year, shift_ind):
    for f in os.listdir(dataset_path):
        if index in f:
            nc_file = f
    file_path = f"{dataset_path}/{nc_file}"
    ds = Dataset(file_path, mode='r')
    times = np.array(ds['time'][:])
    time_index = np.where(times == start_year)[0][0], np.where(times == end_year)[0][0]+1
    data = np.array(ds['Ann'][time_index[0]: time_index[1]])
    ds.close()
    for i in range(len(data)):
        data[i] = _shift_data(data[i], shift_ind)
    return data

def _shift_data(data, shift_index):
    if shift_index:
        data = np.roll(data, shift_index, axis=1)
        return data
    else:
        return data