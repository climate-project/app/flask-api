import numpy as np
import pandas as pd
from .mask_selected import _get_country_mask_arr, _get_custom_mask_arr


def period_avg(annual_period_data):
    return np.round(np.nanmean(annual_period_data, axis=0), 4)


def area_annual_avg(annual_period_data):
    return np.round(np.nanmean(annual_period_data, axis=(1, 2)), 4)