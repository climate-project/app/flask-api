from flask_cors import CORS
from flask import jsonify, request, Blueprint, make_response
from flask import send_file
from app import app
import json

import numpy as np
import pandas as pd
from copy import deepcopy
import datetime
import ast
import os

# ------------------------------
# import my lib
# ------------------------------
from .lib.mymongo import Mongo
from .lib.mylib import roundlist

from .lib import spatial
from .lib import spatial_trend_analysis

# for nc trend analysis
from .lib import my_ncquery
from .lib import spatial_nc
from .lib.upscaling import *

nc_routes = Blueprint('nc_routes', __name__)
database = "climatedb_2019"
nc_path = "./app/ncfiles/nc_raw/"

app.config["CORS_HEADERS"] = "Content-Type"

cors = CORS(app, resources={"/*": {"origins": ["*"]}})

# ----------------------------------------------------------------
# api for query data from nc file
# ----------------------------------------------------------------


def query_period_ncdata(dataset_path, dataset_type, index, start_date, end_date, shift_index):
    if dataset_type != "hist" and start_date <= datetime.datetime.strptime(
            "2005-12-31", "%Y-%m-%d") and end_date > datetime.datetime.strptime("2005-12-31", "%Y-%m-%d"):
        q1 = my_ncquery.query_period_data(dataset_path, "hist", index, start_date,
                                          datetime.datetime.strptime("2005-12-31", "%Y-%m-%d"), shift_index)
        q2 = my_ncquery.query_period_data(dataset_path, dataset_type, index, start_date, end_date, shift_index)
        q = np.append(q1, q2, axis=0)
    else:
        if start_date <= datetime.datetime.strptime("2005-12-31", "%Y-%m-%d"):
            q = my_ncquery.query_period_data(dataset_path, "hist", index, start_date, end_date, shift_index)
        else:
            q = my_ncquery.query_period_data(dataset_path, dataset_type, index, start_date, end_date, shift_index)
    return q


def _lower_resolution(monthly_data, lons, lats):
    new_data = []
    for y in range(len(monthly_data)):
        month_data = []
        for m in range(len(monthly_data[y])):
            month_data.append(upscaling_data(monthly_data[y][m]))
        new_data.append(month_data)
    lons, lats = upscaling_lonlat(lons, lats)
    new_data = np.array(new_data, dtype=np.float)
    return new_data, lons, lats


@nc_routes.route("/api/nc/datasets/<dataset>/types/<dataset_type>/indices/<index>/<start_date>/<end_date>")
def calculate_nc_spatialavg(dataset, dataset_type, index, start_date, end_date):
    dataset_path = f"{nc_path}/{dataset}"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    # query data
    period_data = query_period_ncdata(dataset_path, dataset_type, index, start_date, end_date, shift_index)

    monthly_avg_data = spatial_nc.monthly_avg(period_data)

    period_avg = spatial_nc.period_avg(monthly_avg_data)
    period_avg_iqr = np.nanpercentile(period_avg, 75) - np.nanpercentile(period_avg, 25)
    period_avg_range = [
        round(np.nanpercentile(period_avg, 25) - period_avg_iqr * 1.5, 2),
        round(np.nanpercentile(period_avg, 75) + period_avg_iqr * 1.5, 2),
    ]

    seasonal_avg = np.round(spatial_nc.area_seasonal_avg(monthly_avg_data), 2)
    global_ann_avg = np.round(spatial_nc.area_annual_avg(monthly_avg_data), 2)
    year_list = list(range(int(start_date.year), int(end_date.year) + 1))

    return_json = {
        "map": {
            "avg_map": {
                "data": roundlist(period_avg),
                "min": np.round(np.nanmin(period_avg), 2),
                "max": np.round(np.nanmax(period_avg), 2),
                "avg": np.round(np.nanmean(period_avg), 2),
                "variance": np.round(np.nanvar(period_avg), 2),
                "boxplot_range": period_avg_range,
            }
        },
        "chart": {
            "seasonal": roundlist(seasonal_avg),
            "global_avg": roundlist(global_ann_avg),
            "year": year_list
        },
    }

    return jsonify(return_json)


@nc_routes.route("/api/nc/datasets/<dataset>/types/<dataset_type>/indices/<index>/<start_date>/<end_date>/lowres")
def calculate_nc_spatialavg_lowres(dataset, dataset_type, index, start_date, end_date):
    dataset_path = f"{nc_path}/{dataset}"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lon": 1, "lat": 1})
    query_lonlat = list(query_lonlat)[0]
    lons = np.array(query_lonlat["lon"])
    lats = np.array(query_lonlat["lon"])

    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    # query data
    period_data = query_period_ncdata(dataset_path, dataset_type, index, start_date, end_date, shift_index)

    monthly_avg_data, lons, lats = _lower_resolution(spatial_nc.monthly_avg(period_data), lons, lats)

    period_avg = spatial_nc.period_avg(monthly_avg_data)
    period_avg_iqr = np.nanpercentile(period_avg, 75) - np.nanpercentile(period_avg, 25)
    period_avg_range = [
        round(np.nanpercentile(period_avg, 25) - period_avg_iqr * 1.5, 2),
        round(np.nanpercentile(period_avg, 75) + period_avg_iqr * 1.5, 2),
    ]

    seasonal_avg = np.round(spatial_nc.area_seasonal_avg(monthly_avg_data), 2)
    global_ann_avg = np.round(spatial_nc.area_annual_avg(monthly_avg_data), 2)
    year_list = list(range(int(start_date.year), int(end_date.year) + 1))

    return_json = {
        "map": {
            "avg_map": {
                "data": roundlist(period_avg),
                "min": np.round(np.nanmin(period_avg), 2),
                "max": np.round(np.nanmax(period_avg), 2),
                "avg": np.round(np.nanmean(period_avg), 2),
                "variance": np.round(np.nanvar(period_avg), 2),
                "boxplot_range": period_avg_range,
            }
        },
        "chart": {
            "seasonal": roundlist(seasonal_avg),
            "global_avg": roundlist(global_ann_avg),
            "year": year_list
        },
    }

    return jsonify(return_json)


@nc_routes.route("/api/nc/datasets/<dataset>/types/<dataset_type>/indices/<index>/<start_date>/<end_date>/mannkendall")
def calculate_nc_spatial_mk(dataset, dataset_type, index, start_date, end_date):
    dataset_path = f"{nc_path}/{dataset}"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    # query data
    period_data = query_period_ncdata(dataset_path, dataset_type, index, start_date, end_date, shift_index)

    monthly_avg_data = spatial_nc.monthly_avg(period_data)
    annual_avg_data = spatial_nc.annual_avg(monthly_avg_data)

    mk_test = spatial_trend_analysis.mk_test2(annual_avg_data)

    return jsonify({"mannkendall": roundlist(mk_test, 0)})


@nc_routes.route(
    "/api/nc/datasets/<dataset>/types/<dataset_type>/indices/<index>/<start_date>/<end_date>/mannkendall/lowres")
def calculate_nc_spatial_mk_lowres(dataset, dataset_type, index, start_date, end_date):
    dataset_path = f"{nc_path}/{dataset}"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lon": 1, "lat": 1})
    query_lonlat = list(query_lonlat)[0]
    lons = np.array(query_lonlat["lon"])
    lats = np.array(query_lonlat["lon"])

    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    # query data
    period_data = query_period_ncdata(dataset_path, dataset_type, index, start_date, end_date, shift_index)

    monthly_avg_data, lons, lats = _lower_resolution(spatial_nc.monthly_avg(period_data), lons, lats)

    annual_avg_data = spatial_nc.annual_avg(monthly_avg_data)

    mk_test = spatial_trend_analysis.mk_test2(annual_avg_data)

    return jsonify({"mannkendall": roundlist(mk_test, 0)})


@nc_routes.route(
    "/api/nc/datasets/<dataset>/types/<dataset_type>/indices/<index>/<start_date>/<end_date>/slope/<method>")
def calculate_nc_spatial_theilsen(dataset, dataset_type, index, start_date, end_date, method):
    dataset_path = f"{nc_path}/{dataset}"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    methods = {
        "theilsen": spatial_trend_analysis.theil_sen,
        "linearreg": spatial_trend_analysis.linear_reg,
        "leastsquare": spatial_trend_analysis.least_square
    }

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    # query data
    period_data = query_period_ncdata(dataset_path, dataset_type, index, start_date, end_date, shift_index)

    monthly_avg_data = spatial_nc.monthly_avg(period_data)
    annual_avg_data = spatial_nc.annual_avg(monthly_avg_data)

    slope = methods[method](annual_avg_data)
    slope_iqr = np.nanpercentile(slope, 75) - np.nanpercentile(slope, 25)
    slope_range = [
        round(np.nanpercentile(slope, 25) - slope_iqr * 1.5, 4),
        round(np.nanpercentile(slope, 75) + slope_iqr * 1.5, 4),
    ]

    return jsonify({
        "slope": roundlist(slope, 4),
        "min": np.round(np.nanmin(slope), 4),
        "max": np.round(np.nanmax(slope), 4),
        "avg": np.round(np.nanmean(slope), 4),
        "variance": np.round(np.nanvar(slope), 4),
        "boxplot_range": slope_range,
    })


@nc_routes.route(
    "/api/nc/datasets/<dataset>/types/<dataset_type>/indices/<index>/<start_date>/<end_date>/slope/<method>/lowres")
def calculate_nc_spatial_theilsen_lowres(dataset, dataset_type, index, start_date, end_date, method):
    dataset_path = f"{nc_path}/{dataset}"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    
    methods = {
        "theilsen": spatial_trend_analysis.theil_sen,
        "linearreg": spatial_trend_analysis.linear_reg,
        "leastsquare": spatial_trend_analysis.least_square
    }

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lon": 1, "lat": 1})
    query_lonlat = list(query_lonlat)[0]
    lons = np.array(query_lonlat["lon"])
    lats = np.array(query_lonlat["lon"])

    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    # query data
    period_data = query_period_ncdata(dataset_path, dataset_type, index, start_date, end_date, shift_index)

    monthly_avg_data, lons, lats = _lower_resolution(spatial_nc.monthly_avg(period_data), lons, lats)

    annual_avg_data = spatial_nc.annual_avg(monthly_avg_data)

    slope = methods[method](annual_avg_data)
    slope_iqr = np.nanpercentile(slope, 75) - np.nanpercentile(slope, 25)
    slope_range = [
        round(np.nanpercentile(slope, 25) - slope_iqr * 1.5, 4),
        round(np.nanpercentile(slope, 75) + slope_iqr * 1.5, 4),
    ]

    return jsonify({
        "slope": roundlist(slope, 4),
        "min": np.round(np.nanmin(slope), 4),
        "max": np.round(np.nanmax(slope), 4),
        "avg": np.round(np.nanmean(slope), 4),
        "variance": np.round(np.nanvar(slope), 4),
        "boxplot_range": slope_range,
    })


@nc_routes.route("/api/nc/datasets/<dataset>/types/<dataset_type>/indices/<index>/<start_date>/<end_date>/selectcountry"
                )
def calculate_nc_selected_country_avg(dataset, dataset_type, index, start_date, end_date):
    country = request.headers.get("country")
    title = request.headers.get("title")

    dataset_path = f"{nc_path}/{dataset}"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_period_ncdata(dataset_path, dataset_type, index, start_date, end_date, shift_index)
    monthly_avg_data = spatial_nc.monthly_avg(period_data)

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])

    data = spatial.mask_inside_country(country, lats, lons, monthly_avg_data)
    ann_avg = spatial_nc.area_annual_avg(data)
    seasonal_avg = spatial_nc.area_seasonal_avg(data)

    year_list = list(range(int(start_date.year), int(end_date.year) + 1))

    return jsonify({
        "title": title,
        "country": country,
        "year": year_list,
        "global_avg": roundlist(ann_avg),
        "seasonal": roundlist(seasonal_avg),
    })


@nc_routes.route("/api/nc/datasets/<dataset>/types/<dataset_type>/indices/<index>/<start_date>/<end_date>/selectcustom")
def calculate_nc_selected_custom_avg(dataset, dataset_type, index, start_date, end_date):
    custom_polygon = ast.literal_eval(request.headers.get("custom_polygon"))
    title = request.headers.get("title")

    dataset_path = f"{nc_path}/{dataset}"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])

    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_period_ncdata(dataset_path, dataset_type, index, start_date, end_date, shift_index)
    monthly_avg_data = spatial_nc.monthly_avg(period_data)

    data = spatial.mask_inside_polygon(custom_polygon, lats, lons, monthly_avg_data)
    ann_avg = spatial_nc.area_annual_avg(data)
    seasonal_avg = spatial_nc.area_seasonal_avg(data)
    year_list = list(range(int(start_date.year), int(end_date.year) + 1))

    return jsonify({
        "title": title,
        "polygon": custom_polygon,
        "year": year_list,
        "global_avg": roundlist(ann_avg),
        "seasonal": roundlist(seasonal_avg),
    })


@nc_routes.route("/api/nc/datasets/<dataset>/types/<dataset_type>/indices/<index>/<start_date>/<end_date>/exportcsv")
def export_nc_csv(dataset, dataset_type, index, start_date, end_date):
    dataset_path = f"{nc_path}/{dataset}"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_period_ncdata(dataset_path, dataset_type, index, start_date, end_date, shift_index)

    # flatten period_data
    daily_data = []
    for y in period_data:
        for m in y:
            for d in m:
                daily_data.append(d)
    daily_data = np.array(daily_data)
    daily_data = daily_data.reshape(daily_data.shape[0], daily_data.shape[1] * daily_data.shape[2])
    daily_data = daily_data[~np.isnan(np.nanmean(daily_data, axis=1))]
    date_range = pd.date_range(start_date, end_date)

    df = pd.DataFrame(daily_data)[:len(date_range)]
    df['date'] = date_range
    df = df.reindex(columns=['date'] + list(range(daily_data.shape[1])))

    res = make_response(df.to_csv(index=False))
    res.headers["Content-Disposition"] = f"attachment; filename={dataset}_{dataset_type}_{index}.csv"
    res.headers["Content-Type"] = "text/csv"
    return res


@nc_routes.route(
    "/api/nc/datasets/<dataset>/types/<dataset_type>/indices/<index>/<start_date>/<end_date>/selectcountry/exportcsv")
def export_nc_csv_country(dataset, dataset_type, index, start_date, end_date):
    country = request.headers.get("country")
    title = request.headers.get("title")

    dataset_path = f"{nc_path}/{dataset}"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_period_ncdata(dataset_path, dataset_type, index, start_date, end_date, shift_index)
    period_data = spatial.mask_inside_country_daily(country, lats, lons, period_data)

    # flatten period_data
    daily_data = []
    for y in period_data:
        for m in y:
            for d in m:
                daily_data.append(d)
    daily_data = np.array(daily_data)
    daily_data = daily_data.reshape(daily_data.shape[0], daily_data.shape[1] * daily_data.shape[2])
    daily_data = daily_data[~np.isnan(np.nanmean(daily_data, axis=1))]
    date_range = pd.date_range(start_date, end_date)

    df = pd.DataFrame(daily_data)[:len(date_range)]
    df['date'] = date_range
    df = df.reindex(columns=['date'] + list(range(daily_data.shape[1])))

    res = make_response(df.to_csv(index=False))
    res.headers["Content-Disposition"] = f"attachment; filename={dataset}_{dataset_type}_{index}_{country}.csv"
    res.headers["Content-Type"] = "text/csv"
    return res


@nc_routes.route(
    "/api/nc/datasets/<dataset>/types/<dataset_type>/indices/<index>/<start_date>/<end_date>/selectcustom/exportcsv")
def export_nc_csv_custom(dataset, dataset_type, index, start_date, end_date):
    dataset_path = f"{nc_path}/{dataset}"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    custom_polygon = ast.literal_eval(request.headers.get("custom_polygon"))
    title = request.headers.get("title")

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_period_ncdata(dataset_path, dataset_type, index, start_date, end_date, shift_index)

    period_data = spatial.mask_inside_polygon_daily(custom_polygon, lats, lons, period_data)

    # flatten period_data
    daily_data = []
    for y in period_data:
        for m in y:
            for d in m:
                daily_data.append(d)
    daily_data = np.array(daily_data)
    daily_data = daily_data.reshape(daily_data.shape[0], daily_data.shape[1] * daily_data.shape[2])
    daily_data = daily_data[~np.isnan(np.nanmean(daily_data, axis=1))]
    date_range = pd.date_range(start_date, end_date)

    df = pd.DataFrame(daily_data)[:len(date_range)]
    df['date'] = date_range
    df = df.reindex(columns=['date'] + list(range(daily_data.shape[1])))

    res = make_response(df.to_csv(index=False))
    res.headers["Content-Disposition"] = f"attachment; filename={dataset}_{dataset_type}_{index}_custom.csv"
    res.headers["Content-Type"] = "text/csv"
    return res