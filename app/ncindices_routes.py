from flask_cors import CORS
from flask import jsonify, request, Blueprint, make_response
from flask import send_file
from app import app
import json

import numpy as np
import pandas as pd
from copy import deepcopy
import datetime
import ast
import os

# ------------------------------
# import my lib
# ------------------------------
from .lib.mymongo import Mongo
from .lib.mylib import roundlist
from .lib.my_ncindicesquery import query_index_period_data
from .lib.upscaling import *
# for trend analysis
from .lib import spatial_trend_analysis, spatial_nc, spatial_ncindices, spatial

ncindices_routes = Blueprint('ncindices_routes', __name__)
database = "climatedb_2019"
nc_path = "./app/ncfiles/nc_indices/"

app.config["CORS_HEADERS"] = "Content-Type"

cors = CORS(app, resources={"/*": {"origins": ["*"]}})


def _lower_resolution(data, lons, lats):
    new_data = []
    for y in range(len(data)):
        new_data.append(upscaling_data(data[y]))
    lons, lats = upscaling_lonlat(lons, lats)
    new_data = np.array(new_data, dtype=np.float)
    return new_data, lons, lats


@ncindices_routes.route("/api/ncindices/datasets/<dataset>/indices/<index>/<start_year>/<end_year>")
def query_period_data(dataset, index, start_year, end_year):
    dataset_path = f"{nc_path}/{dataset}"
    start_year = int(start_year)
    end_year = int(end_year)
    year_list = list(range(start_year, end_year + 1))

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_index_period_data(dataset_path, index, start_year, end_year, shift_index)
    period_avg = spatial_ncindices.period_avg(period_data)
    period_avg_iqr = np.nanpercentile(period_avg, 75) - np.nanpercentile(period_avg, 25)
    period_avg_range = [
        round(np.nanpercentile(period_avg, 25) - period_avg_iqr * 1.5, 2),
        round(np.nanpercentile(period_avg, 75) + period_avg_iqr * 1.5, 2),
    ]

    global_annual_avg = spatial_ncindices.area_annual_avg(period_data)

    return_json = {
        "map": {
            "avg_map": {
                "data": roundlist(period_avg),
                "min": np.round(np.nanmin(period_avg), 2),
                "max": np.round(np.nanmax(period_avg), 2),
                "avg": np.round(np.nanmean(period_avg), 2),
                "variance": np.round(np.nanvar(period_avg), 2),
                "boxplot_range": period_avg_range,
            }
        },
        "chart": {
            "seasonal": [],
            "global_avg": roundlist(global_annual_avg),
            "year": year_list
        }
    }

    return jsonify(return_json)


@ncindices_routes.route("/api/ncindices/datasets/<dataset>/indices/<index>/<start_year>/<end_year>/lowres")
def query_period_data_lowres(dataset, index, start_year, end_year):
    dataset_path = f"{nc_path}/{dataset}"
    start_year = int(start_year)
    end_year = int(end_year)
    year_list = list(range(start_year, end_year + 1))

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_index_period_data(dataset_path, index, start_year, end_year, shift_index)

    period_data, lons, lats = _lower_resolution(period_data, lons, lats)

    period_avg = spatial_ncindices.period_avg(period_data)

    period_avg_iqr = np.nanpercentile(period_avg, 75) - np.nanpercentile(period_avg, 25)
    period_avg_range = [
        round(np.nanpercentile(period_avg, 25) - period_avg_iqr * 1.5, 2),
        round(np.nanpercentile(period_avg, 75) + period_avg_iqr * 1.5, 2),
    ]

    global_annual_avg = spatial_ncindices.area_annual_avg(period_data)

    return_json = {
        "map": {
            "avg_map": {
                "data": roundlist(period_avg),
                "min": np.round(np.nanmin(period_avg), 2),
                "max": np.round(np.nanmax(period_avg), 2),
                "avg": np.round(np.nanmean(period_avg), 2),
                "variance": np.round(np.nanvar(period_avg), 2),
                "boxplot_range": period_avg_range
            }
        },
        "chart": {
            "seasonal": [],
            "global_avg": roundlist(global_annual_avg),
            "year": year_list
        }
    }

    return jsonify(return_json)


@ncindices_routes.route("/api/ncindices/datasets/<dataset>/indices/<index>/<start_year>/<end_year>/mannkendall")
def calculate_ncindices_mkt(dataset, index, start_year, end_year):

    dataset_path = f"{nc_path}/{dataset}"
    start_year = int(start_year)
    end_year = int(end_year)
    year_list = list(range(start_year, end_year + 1))

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_index_period_data(dataset_path, index, start_year, end_year, shift_index)
    mk_test = spatial_trend_analysis.mk_test2(period_data)

    return jsonify({"mannkendall": roundlist(mk_test, 0)})


@ncindices_routes.route("/api/ncindices/datasets/<dataset>/indices/<index>/<start_year>/<end_year>/mannkendall/lowres")
def calculate_ncindices_mkt_lowres(dataset, index, start_year, end_year):
    dataset_path = f"{nc_path}/{dataset}"
    start_year = int(start_year)
    end_year = int(end_year)
    year_list = list(range(start_year, end_year + 1))

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_index_period_data(dataset_path, index, start_year, end_year, shift_index)

    period_data, lons, lats = _lower_resolution(period_data, lons, lats)
    mk_test = spatial_trend_analysis.mk_test2(period_data)

    return jsonify({"mannkendall": roundlist(mk_test, 0)})


@ncindices_routes.route("/api/ncindices/datasets/<dataset>/indices/<index>/<start_year>/<end_year>/slope/<method>")
def calculate_ncindices_theilsen(dataset, index, start_year, end_year, method):
    dataset_path = f"{nc_path}/{dataset}"
    start_year = int(start_year)
    end_year = int(end_year)
    year_list = list(range(start_year, end_year + 1))

    methods = {
        "theilsen": spatial_trend_analysis.theil_sen,
        "linearreg": spatial_trend_analysis.linear_reg,
        "leastsquare": spatial_trend_analysis.least_square
    }

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_index_period_data(dataset_path, index, start_year, end_year, shift_index)
    slope = methods[method](period_data)

    slope_iqr = np.nanpercentile(slope, 75) - np.nanpercentile(slope, 25)
    slope_range = [
        round(np.nanpercentile(slope, 25) - slope_iqr * 1.5, 4),
        round(np.nanpercentile(slope, 75) + slope_iqr * 1.5, 4),
    ]

    return jsonify({
        "slope": roundlist(slope, 4),
        "min": np.round(np.nanmin(slope), 4),
        "max": np.round(np.nanmax(slope), 4),
        "avg": np.round(np.nanmean(slope), 4),
        "variance": np.round(np.nanvar(slope), 4),
        "boxplot_range": slope_range,
    })


@ncindices_routes.route(
    "/api/ncindices/datasets/<dataset>/indices/<index>/<start_year>/<end_year>/slope/<method>/lowres")
def calculate_ncindices_theilsen_lowres(dataset, index, start_year, end_year, method):
    dataset_path = f"{nc_path}/{dataset}"
    start_year = int(start_year)
    end_year = int(end_year)
    year_list = list(range(start_year, end_year + 1))

    methods = {
        "theilsen": spatial_trend_analysis.theil_sen,
        "linearreg": spatial_trend_analysis.linear_reg,
        "leastsquare": spatial_trend_analysis.least_square
    }

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_index_period_data(dataset_path, index, start_year, end_year, shift_index)
    period_data, lons, lats = _lower_resolution(period_data, lons, lats)

    slope = methods[method](period_data)

    slope_iqr = np.nanpercentile(slope, 75) - np.nanpercentile(slope, 25)
    slope_range = [
        round(np.nanpercentile(slope, 25) - slope_iqr * 1.5, 4),
        round(np.nanpercentile(slope, 75) + slope_iqr * 1.5, 4),
    ]

    return jsonify({
        "slope": roundlist(slope, 4),
        "min": np.round(np.nanmin(slope), 4),
        "max": np.round(np.nanmax(slope), 4),
        "avg": np.round(np.nanmean(slope), 4),
        "variance": np.round(np.nanvar(slope), 4),
        "boxplot_range": slope_range,
    })



@ncindices_routes.route("/api/ncindices/datasets/<dataset>/indices/<index>/<start_year>/<end_year>/selectcountry")
def query_period_data_country(dataset, index, start_year, end_year):
    country = request.headers.get("country")
    title = request.headers.get("title")

    dataset_path = f"{nc_path}/{dataset}"
    start_year = int(start_year)
    end_year = int(end_year)
    year_list = list(range(start_year, end_year + 1))

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_index_period_data(dataset_path, index, start_year, end_year, shift_index)

    data = spatial.mask_inside_country(
        country, lats, lons, period_data.reshape(period_data.shape[0], 1, period_data.shape[1],
                                                 period_data.shape[2])).squeeze()
    ann_avg = spatial_ncindices.area_annual_avg(data)
    return jsonify({
        "title": title,
        "country": country,
        "year": year_list,
        "global_avg": roundlist(ann_avg),
        "seasonal": roundlist([]),
    })


@ncindices_routes.route("/api/ncindices/datasets/<dataset>/indices/<index>/<start_year>/<end_year>/selectcustom")
def query_period_data_custom(dataset, index, start_year, end_year):
    custom_polygon = ast.literal_eval(request.headers.get("custom_polygon"))
    title = request.headers.get("title")

    dataset_path = f"{nc_path}/{dataset}"
    start_year = int(start_year)
    end_year = int(end_year)
    year_list = list(range(start_year, end_year + 1))

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_index_period_data(dataset_path, index, start_year, end_year, shift_index)

    data = spatial.mask_inside_polygon(
        custom_polygon, lats, lons,
        period_data.reshape(period_data.shape[0], 1, period_data.shape[1], period_data.shape[2])).squeeze()
    ann_avg = spatial_ncindices.area_annual_avg(data)

    return jsonify({
        "title": title,
        "polygon": custom_polygon,
        "year": year_list,
        "global_avg": roundlist(ann_avg),
        "seasonal": roundlist([]),
    })


@ncindices_routes.route("/api/ncindices/datasets/<dataset>/indices/<index>/<start_year>/<end_year>/exportcsv")
def query_period_data_csv(dataset, index, start_year, end_year):
    dataset_path = f"{nc_path}/{dataset}"
    start_year = int(start_year)
    end_year = int(end_year)
    year_list = list(range(start_year, end_year + 1))

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_index_period_data(dataset_path, index, start_year, end_year, shift_index)
    period_data = period_data.reshape(period_data.shape[0], period_data.shape[1] * period_data.shape[2])
    df = pd.DataFrame(period_data)
    df['year'] = year_list
    df = df.reindex(columns=['year', 'month'] + list(range(period_data.shape[1])))
    res = make_response(df.to_csv(index=False))
    res.headers["Content-Disposition"] = f"attachment; filename={dataset}_{index}.csv"
    res.headers["Content-Type"] = "text/csv"
    return res


@ncindices_routes.route(
    "/api/ncindices/datasets/<dataset>/indices/<index>/<start_year>/<end_year>/selectcountry/exportcsv")
def query_period_data_country_csv(dataset, index, start_year, end_year):
    country = request.headers.get("country")
    title = request.headers.get("title")

    dataset_path = f"{nc_path}/{dataset}"
    start_year = int(start_year)
    end_year = int(end_year)
    year_list = list(range(start_year, end_year + 1))

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_index_period_data(dataset_path, index, start_year, end_year, shift_index)
    period_data = spatial.mask_inside_country(
        country, lats, lons, period_data.reshape(period_data.shape[0], 1, period_data.shape[1],
                                                 period_data.shape[2])).squeeze()
    period_data = period_data.reshape(period_data.shape[0], period_data.shape[1] * period_data.shape[2])
    df = pd.DataFrame(period_data)
    df['year'] = year_list
    df = df.reindex(columns=['year', 'month'] + list(range(period_data.shape[1])))
    res = make_response(df.to_csv(index=False))
    res.headers["Content-Disposition"] = f"attachment; filename={dataset}_{index}_{country}.csv"
    res.headers["Content-Type"] = "text/csv"
    return res


@ncindices_routes.route(
    "/api/ncindices/datasets/<dataset>/indices/<index>/<start_year>/<end_year>/selectcustom/exportcsv")
def query_period_data_custom_csv(dataset, index, start_year, end_year):
    custom_polygon = ast.literal_eval(request.headers.get("custom_polygon"))
    title = request.headers.get("title")

    dataset_path = f"{nc_path}/{dataset}"
    start_year = int(start_year)
    end_year = int(end_year)
    year_list = list(range(start_year, end_year + 1))

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    if len(np.where(lons >= 0)[0]):
        shift_index = np.where(lons >= 0)[0][0]
    else:
        shift_index = None

    period_data = query_index_period_data(dataset_path, index, start_year, end_year, shift_index)

    period_data = spatial.mask_inside_polygon(
        custom_polygon, lats, lons,
        period_data.reshape(period_data.shape[0], 1, period_data.shape[1], period_data.shape[2])).squeeze()
    period_data = period_data.reshape(period_data.shape[0], period_data.shape[1] * period_data.shape[2])
    df = pd.DataFrame(period_data)
    df['year'] = year_list
    df = df.reindex(columns=['year', 'month'] + list(range(period_data.shape[1])))
    res = make_response(df.to_csv(index=False))
    res.headers["Content-Disposition"] = f"attachment; filename={dataset}_{index}_custom.csv"
    res.headers["Content-Type"] = "text/csv"
    return res