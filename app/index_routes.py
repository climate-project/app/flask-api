from flask_cors import CORS
from flask import jsonify, request, Blueprint, make_response
from app import app
import json

import numpy as np
from copy import deepcopy
import datetime
import ast
import os
import pandas as pd
# ------------------------------
# import my lib
# ------------------------------
from .lib.mymongo import Mongo
from .lib.mylib import roundlist

# for mongo trend analysis
from .lib import spatial
from .lib import spatial_trend_analysis

index_routes = Blueprint('index_routes', __name__)
database = "climatedb_2019"

app.config["CORS_HEADERS"] = "Content-Type"

cors = CORS(app, resources={"/*": {"origins": ["*"]}})


# api for get spatial selected-period-avg map
# for show in main-map
@index_routes.route("/api/datasets/<dataset>/indices/<index>/<startyear>-<startmonth>/<stopyear>-<stopmonth>")
def calculate_spatialavg(dataset, index, startyear, startmonth, stopyear, stopmonth):
    # query data
    db = Mongo(database=database, collection="{}_{}".format(dataset.lower(), index.lower()))
    query_data = db.find({"year": {"$gte": int(startyear), "$lte": int(stopyear)}})

    data = [i["data"] for i in query_data]

    spatial_annual_avg = spatial.annual_avg(deepcopy(data), int(startmonth), int(stopmonth))
    spatial_avg = spatial.spatial_period_avg(spatial_annual_avg)

    # query baseline and calculate anomaly
    db = Mongo(
        database=database,
        collection="{}_{}_baseline".format(dataset.lower(), index.lower()),
    )
    baseline = list(db.find({"start_year": 1961, "end_year": 1990}))[0]
    baseline_data = np.array(baseline["data"], dtype=np.float)
    annual_baseline = np.round(np.array(baseline["annual_baseline"], dtype=np.float), 2)
    del baseline["data"]

    anomaly = spatial_avg - baseline_data

    # chart
    global_avg = spatial.global_avg(spatial_annual_avg)
    global_anomaly = np.round(global_avg - np.nanmean(baseline_data), 3)

    seasonal_avg = spatial.monthly_avg(deepcopy(data), int(startmonth), int(stopmonth))

    year_list = list(range(int(startyear), int(stopyear) + 1))

    avg_map_iqr = np.nanpercentile(spatial_avg, 75) - np.nanpercentile(spatial_avg, 25)
    avg_map_range = [
        round(np.nanpercentile(spatial_avg, 25) - avg_map_iqr * 1.5, 2),
        round(np.nanpercentile(spatial_avg, 75) + avg_map_iqr * 1.5, 2),
    ]

    anomaly_iqr = np.nanpercentile(anomaly, 75) - np.nanpercentile(anomaly, 25)
    anomaly_range = [
        round(np.nanpercentile(anomaly, 25) - anomaly_iqr * 1.5, 2),
        round(np.nanpercentile(anomaly, 75) + anomaly_iqr * 1.5, 2),
    ]

    return_json = {
        "Selected Option": {
            "dataset": dataset,
            "index": index,
            "start": f"{startyear}-{startmonth}",
            "stop": f"{stopyear}-{stopmonth}",
        },
        "map": {
            "avg_map": {
                "data": roundlist(spatial_avg, 2),
                "min": np.round(np.nanmin(spatial_avg), 2),
                "max": np.round(np.nanmax(spatial_avg), 2),
                "avg": np.round(np.nanmean(spatial_avg), 2),
                "variance": np.round(np.nanvar(spatial_avg), 2),
                "boxplot_range": avg_map_range,
            },
            "anomaly": {
                "data": np.where(np.isnan(anomaly), None, anomaly).tolist(),
                "min": np.round(np.nanmin(anomaly), 2),
                "max": np.round(np.nanmax(anomaly), 2),
                "avg": np.round(np.nanmean(anomaly), 2),
                "variance": np.round(np.nanvar(anomaly), 2),
                "boxplot_range": anomaly_range,
            },
        },
        "chart": {
            "global_avg": roundlist(global_avg, 2),
            "seasonal": roundlist(seasonal_avg, 2),
            "global_anomaly": roundlist(global_anomaly, 3),
            "year": year_list,
            "baseline": roundlist(annual_baseline, 2),
            "baseline_year": list(range(baseline["start_year"], baseline["end_year"] + 1)),
        },
    }

    return jsonify(return_json)


# api for get spatial mann-kendall test
@index_routes.route(
    "/api/datasets/<dataset>/indices/<index>/<startyear>-<startmonth>/<stopyear>-<stopmonth>/mannkendall")
def calculate_spatial_mk(dataset, index, startyear, startmonth, stopyear, stopmonth):
    db = Mongo(database=database, collection="{}_{}".format(dataset.lower(), index.lower()))
    query_data = db.find({"year": {"$gte": int(startyear), "$lte": int(stopyear)}})
    data = [i["data"] for i in query_data]
    spatial_annual_avg = spatial.annual_avg(data, int(startmonth), int(stopmonth))
    mk_test = spatial_trend_analysis.mk_test2(spatial_annual_avg)

    return jsonify({"mannkendall": roundlist(mk_test, 0)})


# api for get spatial thiel-sen test
@index_routes.route("/api/datasets/<dataset>/indices/<index>/<startyear>-<startmonth>/<stopyear>-<stopmonth>/theilsen")
def calculate_spatial_theilsen(dataset, index, startyear, startmonth, stopyear, stopmonth):
    db = Mongo(database=database, collection="{}_{}".format(dataset.lower(), index.lower()))
    query_data = db.find({"year": {"$gte": int(startyear), "$lte": int(stopyear)}})
    data = [i["data"] for i in query_data]
    spatial_annual_avg = spatial.annual_avg(data, int(startmonth), int(stopmonth))
    theilsen_slope = spatial_trend_analysis.theil_sen(spatial_annual_avg)

    theilsen_iqr = np.nanpercentile(theilsen_slope, 75) - np.nanpercentile(theilsen_slope, 25)
    theilsen_range = [
        round(np.nanpercentile(theilsen_slope, 25) - theilsen_iqr * 1.5, 4),
        round(np.nanpercentile(theilsen_slope, 75) + theilsen_iqr * 1.5, 4),
    ]

    return jsonify({
        "theilsen": roundlist(theilsen_slope, 4),
        "min": np.round(np.nanmin(theilsen_slope), 4),
        "max": np.round(np.nanmax(theilsen_slope), 4),
        "avg": np.round(np.nanmean(theilsen_slope), 4),
        "variance": np.round(np.nanvar(theilsen_slope), 4),
        "boxplot_range": theilsen_range,
    })


# api for calculate global avg of selected country area
@index_routes.route(
    "/api/datasets/<dataset>/indices/<index>/<startyear>-<startmonth>/<stopyear>-<stopmonth>/selectcountry")
def calculate_selected_country_avg(dataset, index, startyear, startmonth, stopyear, stopmonth):
    country = request.headers.get("country")
    title = request.headers.get("title")
    # Calculate Global AVG here

    # query data
    db = Mongo(database=database, collection="{}_{}".format(dataset.lower(), index.lower()))
    query_data = db.find({"year": {"$gte": int(startyear), "$lte": int(stopyear)}})

    data = [i["data"] for i in query_data]

    # query lon, lat
    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])

    # select data in country area only
    data = spatial.mask_inside_country(country, lats, lons, data)
    spatial_annual_avg = spatial.annual_avg(deepcopy(data), int(startmonth), int(stopmonth))
    # chart
    global_avg = spatial.global_avg(spatial_annual_avg)
    seasonal_avg = spatial.monthly_avg(deepcopy(data), int(startmonth), int(stopmonth))
    year_list = list(range(int(startyear), int(stopyear) + 1))

    return jsonify({
        "title": title,
        "country": country,
        "year": year_list,
        "global_avg": roundlist(global_avg, 2),
        "seasonal_avg": roundlist(seasonal_avg, 2),
    })


@index_routes.route(
    "/api/datasets/<dataset>/indices/<index>/<startyear>-<startmonth>/<stopyear>-<stopmonth>/selectcustom")
def calculate_selected_custom_avg(dataset, index, startyear, startmonth, stopyear, stopmonth):
    custom_polygon = ast.literal_eval(request.headers.get("custom_polygon"))
    title = request.headers.get("title")
    # Calculate Global AVG here
    # query data
    db = Mongo(database=database, collection="{}_{}".format(dataset.lower(), index.lower()))
    query_data = db.find({"year": {"$gte": int(startyear), "$lte": int(stopyear)}})

    data = [i["data"] for i in query_data]

    # query lon, lat
    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])

    # select data in country area only
    data = spatial.mask_inside_polygon(custom_polygon, lats, lons, data)
    spatial_annual_avg = spatial.annual_avg(deepcopy(data), int(startmonth), int(stopmonth))
    # chart
    global_avg = spatial.global_avg(spatial_annual_avg)
    seasonal_avg = spatial.monthly_avg(deepcopy(data), int(startmonth), int(stopmonth))
    year_list = list(range(int(startyear), int(stopyear) + 1))

    return jsonify({
        "title": title,
        "polygon": custom_polygon,
        "year": year_list,
        "global_avg": roundlist(global_avg, 2),
        "seasonal_avg": roundlist(seasonal_avg, 2),
    })


@index_routes.route("/api/datasets/<dataset>/indices/<index>/<startyear>-<startmonth>/<stopyear>-<stopmonth>/exportcsv")
def export_spatial_period(dataset, index, startyear, startmonth, stopyear, stopmonth):
    # query data
    db = Mongo(database=database, collection="{}_{}".format(dataset.lower(), index.lower()))
    query_data = db.find({"year": {"$gte": int(startyear), "$lte": int(stopyear)}})

    data = [i["data"] for i in query_data]

    spatial_annual_avg = spatial.annual_avg(deepcopy(data), int(startmonth), int(stopmonth))
    year_list = list(range(int(startyear), int(stopyear) + 1))

    spatial_annual_avg = spatial_annual_avg.reshape(spatial_annual_avg.shape[0],
                                                    spatial_annual_avg.shape[1] * spatial_annual_avg.shape[2])

    df = pd.DataFrame(spatial_annual_avg)
    df['year'] = year_list
    df = df.reindex(columns=['year'] + list(range(spatial_annual_avg.shape[1])))

    res = make_response(df.to_csv(index=False))
    res.headers["Content-Disposition"] = f"attachment; filename={dataset}_{index}.csv"
    res.headers["Content-Type"] = "text/csv"

    return res


@index_routes.route(
    "/api/datasets/<dataset>/indices/<index>/<startyear>-<startmonth>/<stopyear>-<stopmonth>/selectcountry/exportcsv")
def export_spatial_period_country(dataset, index, startyear, startmonth, stopyear, stopmonth):
    country = request.headers.get("country")
    title = request.headers.get("title")

    # query data
    db = Mongo(database=database, collection="{}_{}".format(dataset.lower(), index.lower()))
    query_data = db.find({"year": {"$gte": int(startyear), "$lte": int(stopyear)}})

    data = [i["data"] for i in query_data]

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    data = spatial.mask_inside_country(country, lats, lons, data)

    spatial_annual_avg = spatial.annual_avg(deepcopy(data), int(startmonth), int(stopmonth))
    year_list = list(range(int(startyear), int(stopyear) + 1))

    spatial_annual_avg = spatial_annual_avg.reshape(spatial_annual_avg.shape[0],
                                                    spatial_annual_avg.shape[1] * spatial_annual_avg.shape[2])

    df = pd.DataFrame(spatial_annual_avg)
    df['year'] = year_list
    df = df.reindex(columns=['year'] + list(range(spatial_annual_avg.shape[1])))

    res = make_response(df.to_csv(index=False))
    res.headers["Content-Disposition"] = f"attachment; filename={dataset}_{index}_{country}.csv"
    res.headers["Content-Type"] = "text/csv"

    return res



@index_routes.route(
    "/api/datasets/<dataset>/indices/<index>/<startyear>-<startmonth>/<stopyear>-<stopmonth>/selectcustom/exportcsv")
def export_spatial_period_custom(dataset, index, startyear, startmonth, stopyear, stopmonth):
    custom_polygon = ast.literal_eval(request.headers.get("custom_polygon"))
    title = request.headers.get("title")

    # query data
    db = Mongo(database=database, collection="{}_{}".format(dataset.lower(), index.lower()))
    query_data = db.find({"year": {"$gte": int(startyear), "$lte": int(stopyear)}})

    data = [i["data"] for i in query_data]

    db = Mongo(database=database, collection="dataset")
    query_lonlat = db.find({"dataset": dataset}, {"lat": 1, "lon": 1})
    query_lonlat = list(query_lonlat)[0]
    lats = np.array(query_lonlat["lat"])
    lons = np.array(query_lonlat["lon"])
    data = spatial.mask_inside_polygon(custom_polygon, lats, lons, data)


    spatial_annual_avg = spatial.annual_avg(deepcopy(data), int(startmonth), int(stopmonth))
    year_list = list(range(int(startyear), int(stopyear) + 1))

    spatial_annual_avg = spatial_annual_avg.reshape(spatial_annual_avg.shape[0],
                                                    spatial_annual_avg.shape[1] * spatial_annual_avg.shape[2])

    df = pd.DataFrame(spatial_annual_avg)
    df['year'] = year_list
    df = df.reindex(columns=['year'] + list(range(spatial_annual_avg.shape[1])))

    res = make_response(df.to_csv(index=False))
    res.headers["Content-Disposition"] = f"attachment; filename={dataset}_{index}_custom.csv"
    res.headers["Content-Type"] = "text/csv"

    return res
