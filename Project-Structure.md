# Project Structure

## Table of Content

- [Project Structure](#Project-Structure) การจัดวางตำแหน่งไฟล์แบบคร่าวๆ
- [NC file Format](#NC-file-Format) ฟอร์แมทของไฟล์ nc เพื่อให้สามารถอ่านไฟล์ได้อย่างถูกต้อง
- [Code](#Code) การจัดวางตำแหน่งของไฟล์โค้ด แต่ละไฟล์ใช้ทำอะไรบ้าง

## Project Structure

รูปแบบการจัดวางไฟล์ทั้งหมดในโปรเจคเป็นดังนี้ เมื่อทำการ clone project ไปจะมีแต่ไฟล์โค้ด จึงต้องทำการวางไฟล์ nc ให้ถูกต้องตามตำแหน่ง

```
Project-Folder:
|   .gitignore
|   runserver.py
|
\---app
    |   index_routes.py
    |   ncindices_routes.py
    |   nc_routes.py
    |   routes.py
    |
    +---lib
    |   |   mask_selected.py
    |   |   mylib.py
    |   |   mymaplib.py
    |   |   mymongo.py
    |   |   my_ncindicesquery.py
    |   |   my_ncquery.py
    |   |   NCFile.py
    |   |   spatial.py
    |   |   spatial_nc.py
    |   |   spatial_ncindices.py
    |   |   spatial_trend_analysis.py
    |   |   upscaling.py
    |   |
    |   \---data
    |           geo-medium.json
    |           MPI-ESM-MR_land_mask.csv
    |           result_1577159469.png
    |           result_1577257473.png
    |
    \---ncfiles
        |   import_index_details.py
        |   import_metadata_indices_nc.py
        |   import_metadata_raw_nc.py
        |   import_metadata_raw_ncrcm.py
        |
        +---nc_indices
        |   +---ecearth_rcp45
        |   |       ecearth_rcp45_CDD.nc
        |   |       ecearth_rcp45_CSDI.nc
        |   |       ecearth_rcp45_TXn.nc
        |   |       ecearth_rcp45_TXx.nc
        |   |       ecearth_rcp45_WSDI.nc
        |   |
        |   +---ecearth_rcp85
        |   |       ecearth_rcp85_CDD.nc
        |   |
        |   +---hadgem2_rcp45
        |   |       hadgem2_rcp45_CDD.nc
        |   |
        |   +---mpi_rcp45
        |   |       mpi_rcp45_CDD.nc
        |   |
        |   \---mpi_rcp85
        |           mpi_rcp85_CDD.nc
        |
        +---nc_raw
        |   \---MPI-ESM-MR
        |       +---hist
        |       |       T2m_pr_day_MPI-ESM-MR_historical.197001.nc
        |       |       T2m_pr_day_MPI-ESM-MR_historical.197002.nc
        |       |       ...
        |       |       T2m_pr_day_MPI-ESM-MR_historical.200509.nc
        |       |       T2m_pr_day_MPI-ESM-MR_historical.200510.nc
        |       |       T2m_pr_day_MPI-ESM-MR_historical.200511.nc
        |       |       T2m_pr_day_MPI-ESM-MR_historical.200512.nc
        |       |
        |       +---rcp45
        |       |       T2m_pr_day_MPI-ESM-MR_rcp45.200601.nc
        |       |       ...
        |       |       T2m_pr_day_MPI-ESM-MR_rcp45.210010.nc
        |       |       T2m_pr_day_MPI-ESM-MR_rcp45.210011.nc
        |       |       T2m_pr_day_MPI-ESM-MR_rcp45.210012.nc
        |       |
        |       \---rcp85
        |               T2m_pr_day_MPI-ESM-MR_rcp85.200601.nc
        |               ...
        |               T2m_pr_day_MPI-ESM-MR_rcp85.210010.nc
        |               T2m_pr_day_MPI-ESM-MR_rcp85.210011.nc
        |               T2m_pr_day_MPI-ESM-MR_rcp85.210012.nc
        |
        +---_data
        |       index_definitions.csv
        |
        \---_metadata_lib
                cartesian_product.py
                geojson_gen.py
                shift.py
```

### /app

เก็บข้อมูลและโค้ดของโปรเจคทั้งหมด

### /app/lib

เก็บโค้ดสำหรับอ่านข้อมูลจาก ncfile และการทำ trend analysis

### /app/ncfiles

เก็บไฟล์ dataset แบ่งเป็น 2 แบบได้แก่

- `nc_indices` เก็บไฟล์ประเภท Extreme index (รายปี)
- `nc_raw` เก็บไฟล์ประเภทข้อมูลดิบ (รายวัน)

เก็บโค้ดสำหรับการ import metadata เข้า MongoDB

`/app/ncfiles/_data` เก็บไฟล์ CSV ข้อมูล, ความหมาย รายละเอียด ของ extreme index, climate variable แต่ละอัน

## NC file Format

### Extreme index

ไฟล์ nc ของข้อมูล extreme index จะถูกเก็บไว้ในโฟลเดอร์ `/app/ncfiles/nc_indices` โดยมี format ดังนี้

โค้ดจะทำการหา dataset จากชื่อโฟลเดอร์ และทำการเลือกอ่านไฟล์จากการดูว่า มี index อยู่ในชื่อไฟล์หรือไม่

```
+---nc_indices
|   +---dataset_name (ชื่อโฟลเดอร์เป็นชื่อของ dataset เช่น ecearth_rcp45 = ชุดข้อมูล ec_earth, pathway แบบ rcp45)
|           dataset_name_CDD.nc (ข้อมูล index CDD)
|           dataset_name_<index_name>.nc
```

รูปแบบการเก็บข้อมูลในไฟล์ nc เนื่องจากข้อมูล extreme index ที่ได้มาเป็นรูปแบบไฟล์ txt จึงต้องทำการแปลงเป็นรูปแบบไฟล์ nc ดังนี้

```python
from netCDF4 import Dataset
ds = Dataset("ecearth_rcp45_CDD.nc")

# ds
<class 'netCDF4._netCDF4.Dataset'>
root group (NETCDF4 data model, file format HDF5):
    dimensions(sizes): lat(191), lon(253), time(130)
    # ข้อมูลจะถูกเก็บใน variable Ann
    variables(dimensions): float64 lat(lat), float64 lon(lon), int64 time(time), float64 Ann(time,lat,lon)
    groups:
```

[txt to nc repository](https://github.com/chuan-khuna/txt_indices_to_nc)

### Raw Data

ไฟล์ nc ของข้อมูล raw data จะถูกเก็บไว้ในโฟลเดอร์ `/app/ncfiles/nc_raw` ข้อมูลดิบจะถูกแบ่งออกเป็น 3 โฟลเดอร์ย่อยๆ ได้แก่ hist, rcp45 และ rcp85 โดย rcp (ข้อมูลในอนาคต) คือ pathway ที่แสดงถึงการปล่อยคาร์บอนต่างๆ

เนื่องจากไฟล์ข้อมูลดิบ จากแต่ละที่มีความแตกต่างกันเล็กน้อย เช่น

- การตั้งชื่อไฟล์
- จำนวนไฟล์ (บางชุดข้อมูลมีไฟล์ไม่ครบปี)
- ข้อมูลตัวแปรสภาพอากาศในไฟล์ (บางชุดไม่มีข้อมูล tas)
- ข้อมูลชื่อตัวแปรในไฟล์ (บางชุดเรียก lat, lon ไม่เหมือนกัน)

การ import metadata และ การที่จะทำให้โปรแกรมสามารถอ่านข้อมูลดิบได้ถูกต้อง จึงมีรายละเอียดเล็กน้อยที่ต้องสังเกตมากกว่าข้อมูล extreme index ที่ได้ทำการแปลงเอง

format ของการเก็บไฟล์ข้อมูลดิบ โดยชื่อไฟล์ต้องลงท้ายด้วยรูปแบบ `yyyymm` เนื่องจากโค้ดการอ่านไฟล์จะทำการอ่านไฟล์สำหรับช่วงเวลาที่เลือกจากชื่อไฟล์

```
+---nc_raw
|   \---MPI-ESM-MR
|       +---hist (ข้อมูลอดีต จะเป็นปี 1970-2005)
|       |       T2m_pr_day_MPI-ESM-MR_historical.197001.nc
|       |       ...
|       |       T2m_pr_day_MPI-ESM-MR_historical.200512.nc
|       |
|       +---rcp45 (ข้อมูลอนาคตแบบ rcp45 จะเป็นปี 2006-2100)
|       |       T2m_pr_day_MPI-ESM-MR_rcp45.200601.nc
|       |       ...
|       |       T2m_pr_day_MPI-ESM-MR_rcp45.210012.nc
|       |
|       \---rcp85
|               T2m_pr_day_MPI-ESM-MR_rcp85.200601.nc
|               ...
|               T2m_pr_day_MPI-ESM-MR_rcp85.210012.nc
```

- **หากชุดข้อมูลที่ได้มา มีไฟล์ไม่ครบปี** (เช่น มีถึง 2100-11) จะทำให้การอ่านไฟล์เกิดปัญหา เนื่องจากโค้ดถูกเขียนไว้แบบ ให้ทำการอ่านให้ครบปี แล้วทำการ fill nan ในช่วงเวลาที่ไม่ได้เลือกไว้ เพื่อสะดวกในการใช้ parameter axis ของ numpy **การแก้ไข** ทำได้โดยลบไฟล์ทิ้งให้ครบช่วงปี หรือ นำไฟล์ที่มีอยู่มาเปลี่ยนชื่อให้เป็นปีนั้นๆ ที่ขาดหายไป (ทำหลัง import metadata เพื่อให้ช่วงเวลาเดิมยังถูกต้อง)

- **ตรวจสอบข้อมูลใน hist และ RCP** ว่ามีไฟล์ซ้ำกันหรือไม่ เช่น ใน RCP มี 200512 ให้ทำการลบทิ้ง

- **ชุดข้อมูล HadGEM2 ที่ 1 ปี มี 360วัน (ทุกเดือนมี 30 วัน)** ต้องทำการแก้ไขข้อมูลวันที่เริ่มต้นและสิ้นสุดใน database เอง

## Code

การทำงานหลักๆ ของ backend ได้แก่

- อ่านข้อมูลจากไฟล์ และ ทำการ shift ข้อมูล ร่วมกับข้อมูล lon, lat จาก database

```
การอ่านไฟล์ข้อมูลดิบจะทำการอ่านเป็นปีเริ่มต้นจนถึงปีสิ้นสุด ทุกวัน จากนั้นค่อยมาทำการ เปลี่ยนให้เป็น nan ในช่วงวันที่ไม่ได้เลือก เพื่อความสะดวกในการใช้ axis ของ numpy

เนื่องจากในไฟล์ nc ค่า lon จะเป็น 0..360 แต่ การพล็อตข้อมูลลงแผนที่จะใช้เป็น -180..180 ข้อมูลจะต้องถูก roll

ถ้าหากเป็นข้อมูล RCM ที่ค่า lon ไม่เกิน 180 ไม่ต้อง shift ข้อมูล
```

- ถ้าหากมีการเรียกใช้งานแบบ low res ข้อมูลที่ถูก shift และ lon, lat จะถูกทำให้มีความละเอียดน้อยลง

- ถ้าหากเป็นข้อมูลดิบ จะถูกนำมาเฉลี่ยเป็นรายเดือน ทำให้ได้มิติเป็น `(ปี, เดือน, #map[y, x])`

```
เนื่องจากจำนวนวันที่ในแต่ละเดือนมีไม่เท่ากัน 28, 30, 31 การเฉลี่ยเตรียมไว้สำหรับ trend analysis และ chart
```

- ถ้าหากมีการเลือกประเทศ หรือพื้นที่ที่สนใจ จะทำการสร้าง mask array แล้วนำ mask array ไปทำการเลือกเฉพาะพื้นที่ที่ถูก mask = 1 ไว้ ข้อมูลนอกพื้นที่จะถูกทำให้เป็น nan

- นำข้อมูลเฉลี่ยรายปี จะได้มิติเป็น `(ปี, #map[y, x])` เพื่อนำไปทำ trend analysis

```
การทำ mann-kendall test มีข้อจำกัดเรื่องข้อมูลที่เป็น seasonal จึงต้องทำเป็นข้อมูลแบบรายปีก่อน
```

- นำข้อมูลไปทำเป็น chart หรือ trend analysis

- return ข้อมูลให้ frontend หรือ ทำการแปลงเป็นไฟล์ให้ user download ถ้า user รีเควสแบบ export file

```
Trend Analysis มีการคำนวณที่ใช้เวลานาน, การดึงข้อมูลเยอะกิน RAM มาก
ข้อมูล RCM ประมาณ 30 ปี

Mann-Kendall ใช้เวลา 40s
Theil-Sen ใช้เวลา 400s
LinearRegression ใช้เวลา 5s
LeastSquare ใช้เวลา 2s
(theil-sen, linear regression, least square ให้ผลใกล้เคียงกัน)


สำหรับข้อมูล 30 ปี
ใช้พื้นที่ RAM ประมาณ 5GB-10GB
```


### app/

ไฟล์ในโฟลเดอร์นี้จะเป็นไฟล์สำหรับ api มีการเรียกใช้งานฟังก์ชันจากไฟล์อื่นๆ (`app/lib/`) ในการเข้าถึง database, การอ่านไฟล์, trend analysis แล้วทำการเตรียมข้อมูลเพื่อรีเทิร์นให้กับ front end

```
\---app
    |   index_routes.py (จากโปรเจคเก่า - เลิกใช้งาน ใช้การอ่านจาก nc แทน)
    |   ncindices_routes.py (api route ของข้อมูล extreme index)
    |   nc_routes.py (api route ข้อข้อมูลดิบ)
    |   routes.py (api route หลักๆ เกี่ยวกับ metadata มีการรวม route ย่อย)
```

### app/lib/

```
\---app
    |
    +---lib
        |   mask_selected.py (สำหรับทำการสร้าง mask array ในพื้นที่ที่เลือก)
        |   mylib.py (สำหรับแปลงข้อมูลเพื่อ รีเทิร์น json)
        |   mymaplib.py (ไม่ได้ใช้งาน)
        |   mymongo.py (สำหรับอ่านข้อมูลจาก mongodb)
        |   my_ncindicesquery.py (สำหรับ query ข้อมูลไฟล์ nc แบบ index)
        |   my_ncquery.py (สำหรับ query ข้อมูลไฟล์ nc แบบ raw data)
        |   NCFile.py (ไม่ได้ใช้งาน)
        |   spatial.py
        |   spatial_nc.py (การหาค่าเฉลี่ยมิติต่างๆของข้อมูลดิบ)
        |   spatial_ncindices.py (การหาค่าเฉลี่ยมิติต่างๆของข้อมูล index)
        |   spatial_trend_analysis.py
        |   upscaling.py (แปลงกริดให้มีรายละเอียดน้อยลง เพื่อความรวดเร็ว)
        |
        \---data
                geo-medium.json (ข้อมูลโพลิกอนรูปร่างของประเทศต่าง)
```

### app/lib/spatial

ประกอบด้วยฟังก์ชันที่สามารถใช้กับข้อมูลทั้งแบบ raw และ index ฟังก์ชั่นส่วนมากเกี่ยวกับข้อมูลที่อยู่ในรูปแบบรายปีแล้ว นำไปแสดงผลเป็นรูปแบบกราฟ เช่น

- กราฟ annual avg
- กราฟ seasonal avg
- กราฟ annual, seasonal ของประเทศที่เลือก

### app/lib/spatial_trend_analysis

ประกอบด้วยฟังก์ชันสำหรับทำ trend analysis ของข้อมูลแบบรายปี ได้แก่ Theil-Sen Slope และ Mann-Kendall test

### app/ncfiles/(import metadata)

```
\---ncfiles
    |   import_index_details.py
    |   import_metadata_indices_nc.py
    |   import_metadata_raw_nc.py (ไม่ได้ใช้แล้ว)
    |   import_metadata_raw_ncrcm.py
```

ประกอบด้วยสคริปสำหรับการ import metadata เข้า mongodb

- `import_index_details` จะเป็นการ import ข้อมูลเกี่ยวกับ index, variable(ในข้อมูลดิบ) เช่น ตัวย่อ ความหมาย หน่วย สีที่ใช้ สามารถแก้ไขไฟล์ csv ได้ใน `/app/ncfiles/_data/index_definitions.csv`

- `import_metadata_indices_nc` จะเป็นการ import metadata ของข้อมูลประเภท index

- `import_metadata_raw_ncrcm` จะเป็นการ import metadata ของข้อมูลประเภทข้อมูลดิบ แบบ RCM และ GCM

ข้อควรระวัง เนื่องจากข้อมูลมีหลาย format จึงต้องทำการตรวจดูอย่างละเอียดเรื่อง variable ที่ต้องการ import หรือทั้งตัวแปรในด้านพิกัด

```
สังเกตบรรทัด

INDICES = list(ds.variables.keys())[-4:]

[-4:] เป็นการเลือกเฉพาะ variable ที่ต้องการ (tas, tasmax, tasmin, pr) โดยไม่นำ variable ด้านพิกัดมา บาง dataset อาจจะต้องมีการเปลี่ยนแปลงค่า -4 เป็นค่าอื่น
```
